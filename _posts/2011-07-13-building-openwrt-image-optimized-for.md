---
layout: single
title: Building an OpenWRT image optimized for MP3 audio playback
date: '2011-07-13T20:36:00.010+02:00'
author: Michael Rumpf
tags:
- audio
- linux
- openwrt
modified_time: '2011-07-24T13:19:48.729+02:00'
categories:
---

For the audio-renderer, see revious posts, I decided to switch to the trunk version of OpenWRT.  
I experienced some issues with the USB audio hardware and the trunk version should prove that there 
are no issues with the USB audio drivers which are already fixed.  
The following configuration is for the bleeding-edge version, some module names have changed so it 
cannot be applied to the Kamikaze (10.3) version without modification (e.g. block-mount contains the 
overlay functionality and kmod-fs-ext4 has support for all ext file-system versions).  
**NOTE: This configuration has stripped all modules which are not necessary for USB audio and Wireless LAN.**  

````
Target System: Atheros AR71xx/AR7240/AR913x/AR934x)  
Target Profile: TP-Link TL-WR1043ND v1  

Global build settings --->  
  [*] Remove ipkg/opkg status/data files in final images  
  [ ] Enable ipv6 support in packages  

LATER: Strip unnecessary exports from kernel image  
LATER: Strip unnecessary functions from libraries  

[*] Build the OpenWRT Image Builder  
[*] Build the OpenWRT SDK  
[*] Build the OpenWRT based toolchain  
[*] Image configuration --->  
    LAN Gateway: 192.168.0.5  
    LAN Network Mask: [255.255.255.0](tel:255.255.255.0)  
    LAN IP Address: 192.168.0.111  

Base system  
  <*> block-mount  
  < > dnsmasq  
  < > firewall  

Kernel Modules
  Filesystems   
    <*> kmod-fs-ext4 (for USB drive)  

  Native Language Support  
    <*> kmod-nls-utf8

  Sound Support  
    <*> kmod-sound-core  
    <*> kmod-usb-audio  

  USB Support  
    <*> kmod-usb-ohci  
    <*> kmod-usb-storage (for USB drive)  

  Other Modules (not sure yet whether they are needed)  
    ??? *gpio* ??? (Buttons for preset-switching ???)  
   
Network
  < > ppp

  Firewall
    < > iptables

Time Synchronization 
    <*> ntpclient

Multimedia  
  <*> gst-mod-alsa
  <*> gst-mod-mad
  <*> gst-mod-souphttpsrc
  <*> gstreamer
  <*> gstreamer-utils (for gst-* tools)

Utilities
  <*> alsa-utils (for alsamixer/alsactl)
````

After the configuration step is completed, the image can be compiled with the following command:
````
make -j 5 # on a quad-core machine
````
  
With the configuration above the compiled image is 5248kB large, which means 2944kB are free for modules and configuration in the /overlay file-system.  
Then you can connect to the router with the following command (assuming that OpenWRT is already installed):

````
telnet 192.168.0.111
````

Copy the new image to a location on the router with enough space (I use a 32MB USB stick with ext2 file-system which I mount under /mnt/usb):
````
scp USERNAME@HOST:HOME/openwrt-trunk/bin/ar71xx/openwrt-ar71xx-generic-tl-wr1043nd-v1-squashfs-sysupgrade.bin .  
````
  
Now drop the caches:
````
echo 3 > /proc/sys/vm/drop_caches  
````  
  
Then reflash the image with the following command:

````
mtd -r write openwrt-ar71xx-generic-tl-wr1043nd-v1-squashfs-sysupgrade.bin firmware
````

The router will reboot and when connecting via telnet again, the setup can be tested with the following command:

````
gst-launch souphttpsrc location=http://87.248.219.60/stream/swr_mp3_m_swr1bwa ! mad ! alsasink  
````

Et viola, the USB audio device will play the radio station! Hopefully..... ;)
