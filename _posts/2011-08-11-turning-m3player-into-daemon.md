---
layout: single
title: Turning m3player into a daemon
date: '2011-08-11T21:29:00.002+02:00'
author: Michael Rumpf
tags:
- audio
- openwrt
modified_time: '2012-07-17T23:46:32.147+02:00'
categories:
---

As most of the public HOWTOs seem to be either incomplete or a bit outdated and because I 
have no copy of the book 
[Advanced Programming in the UNIX Environment](http://www.amazon.com/Programming-Environment-Addison-Wesley-Professional-Computing/dp/0201563177), 
Instead I found the following microHOWTOs:  

*   [Cause a process to become a daemon](http://www.microhowto.info/howto/cause_a_process_to_become_a_daemon.html)
*   [Cause a process to become a daemon in c](http://www.microhowto.info/howto/cause_a_process_to_become_a_daemon_in_c.html)

By following the steps, described there I was able to turn 
[m3player](http://www.jcoderz.org/m3dditiez/wiki/m3ddity-player) into a daemon. 
The daemon behavior can be turned on by passing the `-d` or `--daemon` switches to the command 
line. When the daemon switch is specified, all output (stdout and stderr) gets redirected into 
the log file at the default location `/var/log/m3player.log` or to the location, specified by 
the `-l` or `--logfile` switch.