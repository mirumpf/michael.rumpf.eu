---
layout: single
title: Make ALSA settings persistent on OpenWRT
date: '2011-07-13T21:36:00.003+02:00'
author: Michael Rumpf
tags:
- audio
- openwrt
modified_time: '2011-07-17T18:32:26.194+02:00'
categories:
---

The command-line tool alsamixer can be used to control the volume of the USB audio output:  

[![](http://2.bp.blogspot.com/-EHJmq1aKXrI/Th3tP-sqcLI/AAAAAAAAEls/SGdLN4yRHo4/s320/alsamixer.png)](http://2.bp.blogspot.com/-EHJmq1aKXrI/Th3tP-sqcLI/AAAAAAAAEls/SGdLN4yRHo4/s1600/alsamixer.png)

Unfortunately the setting does not survive a reboot. Another tool from the alsa-utils package can be used for this purpose: alsactl  

After setting the volume with alsamixer, the following command persists the current setting in the file-system for device 0:  
````
alsactl -f /etc/alsa/alsa0.state store 0  
````

During startup, the state must be restored again. This can be achieved by adding the following command to /etc/rc.local:  
````
alsactl -f /etc/alsa/alsa0.state restore 0
````