---
layout: single
title: 'GUPNP: Failed to get SCPD: Cannot resolve hostname ((null))'
date: '2011-06-05T18:23:00.003+02:00'
author: Michael Rumpf
tags:
- linux
- upnp
- ubuntu
modified_time: '2011-07-17T18:54:46.130+02:00'
categories:
---

The following message occurs when the `<urlbase></urlbase>` tag of the root device description is empty when trying to autoconnect actions:  

````
** (process:25888): WARNING **: Failed to get SCPD: Cannot resolve hostname ((null))  
````

Changing it to **http://localhost** fixes the issue.  

````
<?xml version="1.0"?>  
<root xmlns="urn:schemas-upnp-org:device-1-0">  
  <specVersion>  
    <major>1</major>  
    <minor>0</minor>  
  </specVersion>  
 <URLBase>http://localhost</URLBase>
  <device>  
    <!-- ... -->  
  </device>  
</root>  
````

The message is a bit misleading. I was first trying to find an issue with my DNS configuration.