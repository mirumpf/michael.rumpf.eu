---
layout: single
title: 'Maven 3: SSH deployment to foreign repository'
date: '2011-12-31T16:08:00.000+01:00'
author: Michael Rumpf
tags:
- ssh
- maven
- java
modified_time: '2012-01-01T09:44:34.977+01:00'
categories:
---

  
Using the maven-deploy-plugin to deploy artifacts to a repository that is not the
original target repository of a project is getting complicated with Maven 3 when 
the wagon SSH provider is not part of the Maven distribution. The following JAR 
files must be added to the Maven distribution into the `lib/ext` extension folder: 
* jsch-0.1.44-1.jar
* plexus-utils-3.0.jar
* plexus-interactivity-api-1.0-alpha-6.jar
* wagon-ssh-2.2.jar\n* wagon-ssh-external-2.2.jar
* wagon-ssh-common-2.2.jar

Then the parent `pom.xml` of the project must be extended by the  following lines: 

````
    <dependecy>
        <groupid>org.apache.maven.plugins</groupid>
        <artifactid>maven-deploy-plugin</artifactid>
    </dependecy>
    <dependecy>
        <groupid>org.apache.maven.wagon</groupid>
        <artifactid>wagon-ssh</artifactid>                
        <version>2.2</version>
    </dependecy>
````

The `settings.xml` contains the user credentials and looks like this:    
````
        <servers>      
            <server><id>myrepo-snapshots</id>         
                 <username>username</username>          
                 <privatekey>${user.home}/.ssh/id_rsa</privatekey>         
                <passphrase>key-passphrase</passphrase>          
                <filepermissions>666</filepermissions>          
                <directorypermissions>777</directorypermissions>
            </server> 
        </servers>  
````

Then the deploy phase can be executed like this:

````
mvn deploy -DaltDeploymentRepository=myrepo-snapshots::default::scp://host:port/var/www/repos/maven2-snapshot/
````