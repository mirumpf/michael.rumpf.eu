---
layout: single
title: Wake On LAN
date: '2012-03-03T22:50:00.002+01:00'
author: Michael Rumpf
tags:
- ubuntu
- linux
- wakonlan
modified_time: '2012-03-09T22:38:54.032+01:00'
categories:
---

Under Ubuntu I chose the following way to activate wake on LAN on the eth0 device:  

````
# Install ethtool  
sudo apt-get install ethtool  

# Create init script to set the wol bit on the device  
sudo vi /etc/init.d/wakeonlan  
  
  
#!/bin/sh  
ethtool -s eth0 wol g  
exit  

# Make the script executable  
sudo chmod a+x /etc/init.d/wakeonlan  

# Create the init links  
sudo update-rc.d wakeonlan defaults  
````
  
Under Debian the following approach has been chosen:  
````
# Get root  
sudo su -  

# Edit the network interface configuration  
vi /etc/network/interfaces  

# Add the lines post-up/pre-down to your network device section  
iface eth0 inet dhcp  
        post-up /usr/sbin/ethtool -s $IFACE wol g  
        pre-down /usr/sbin/ethtool -s $IFACE wol g  
````

The `post-up/post-down´ commands execute the ethtool each time the eth0 device gets started or stopped. This happens also during boot (post-up) and shutdown (pre-down).

Both approaches activate WOL on the network device so that the machine can be started by sending a WOL package.