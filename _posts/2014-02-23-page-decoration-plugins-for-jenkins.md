---
layout: single
title: Page decoration plugins for Jenkins, SonarQube, and Nexus
date: '2014-02-23T18:34:00.000+01:00'
author: Michael Rumpf
tags:
- jenkins
- nexus
- sonarqube
modified_time: '2014-02-23T18:34:35.644+01:00'
categories:
---

While trying to apply a unified visual layout to the tools [Jenkins](http://jenkins-ci.org/), 
[SonarQube](http://www.sonarqube.org/), and [Nexus](http://www.sonatype.org/nexus/) I learned that 
there are some plugins available, but none of them really fits the intended purpose.

Jenkins has no easy way of changing its appearance and the 
[Simple Theme plugin](https://wiki.jenkins-ci.org/display/JENKINS/Simple+Theme+Plugin) comes only 
close to what is needed. In SonarQube there is a 
[Branding plugin](http://docs.codehaus.org/display/SONAR/Branding+Plugin) with which you can display 
an image at the top of each page, but that's basically all. The Nexus plugin to change the 
[Sonatype Nexus logo is available for the professional version only](http://blog.sonatype.com/2010/02/custom-logo-nxpro/), 
but buying the professional version just for being able to change the logo is not really an option.

Analyzing the APIs of the different tools revealed that similar extension points 
are offered in order to inject HTML fragments into the HTML page:
*   **Jenkins:** hudson.model.PageDecorator
*   **SonarQube (since 3.3):** org.sonar.api.web.PageDecoration
*   **Nexus:** org.sonatype.nexus.plugins.rest.NexusIndexHtmlCustomizer

For Jenkins and SonarQube I found plugins which use these extension points, although the 
plugins are not available via the official plugin repositories and therefore somewhat hidden from the public:
*   **Jenkins:** [page-markup](https://wiki.jenkins-ci.org/display/JENKINS/Page+Markup+Plugin) ([GitHub](https://github.com/mrumpf/page-markup))  
    The [original Subversion repository](https://software.sandia.gov/trac/fast/wiki/PageMarkup) has been [forked to GitHub](https://github.com/bklang/page-markup) but the plugin has not made it into the Jenkins Plugin Repository yet.
*   **SonarQube:** [sonar-pagedecoration-plugin](https://github.com/fcrespel/sonar-pagedecoration-plugin) ([GitHub](https://github.com/mrumpf/sonar-pagedecoration-plugin))  
    It seems as if there has been no attempt at getting the plugin into the [SonarQube Plugin Forge](http://docs.codehaus.org/display/SONAR/Plugin+Library) yet.
*   **Nexus:** No plugin available or at least I did not find one yet.

Inspired from the Blog postings 
"[Nexus and UI contributions](http://hanzelmann.de/blog/2012/09/13/nexus-plugins-and-ui-contributions/)" 
and "[Extending Nexus UI](http://hanzelmann.de/blog/2012/09/16/extending-nexus-navigation/)" I decided 
to write my own plugin which matches the functionality of the SonarQube's pagedecoration and the Jenkins's 
page-markup plugins.

The new [nexus-pagedecoration-plugin](https://github.com/mrumpf/nexus-pagedecoration-plugin) adds a 
menu item "**Page Decoration**" to the Administration section of the Nexus UI:

[![](http://2.bp.blogspot.com/-Ni1gkHRtdxA/UwoGugngvGI/AAAAAAAAcZQ/ZySSX11ac-8/s1600/2014-02-23+15_33_10-Clipboard.png)]
(http://2.bp.blogspot.com/-Ni1gkHRtdxA/UwoGugngvGI/AAAAAAAAcZQ/ZySSX11ac-8/s1600/2014-02-23+15_33_10-Clipboard.png)

The corresponding Page Decoration tab provides text areas for all the four extension points:
*   Pre Head
*   Post Head
*   Pre Body
*   Post Body

You can add HTML fragments to each of the text areas and when saving the changes and reloading the page, the 
fragments will be rendered into the Nexus UI HTML page.

[![](http://1.bp.blogspot.com/-dzOV41d622g/UwoHbwRdt_I/AAAAAAAAcZY/WwVimvnJwfs/s1600/2014-02-23+15_35_57-Sonatype+Nexus.png)]
(http://1.bp.blogspot.com/-dzOV41d622g/UwoHbwRdt_I/AAAAAAAAcZY/WwVimvnJwfs/s1600/2014-02-23+15_35_57-Sonatype+Nexus.png)

The [nexus-pagedecoration-plugin](https://github.com/mrumpf/nexus-pagedecoration-plugin) is available under the 
[Apache 2.0 license](http://www.apache.org/licenses/LICENSE-2.0.html) and is hosted on Github. The initial release for 
Nexus 2.7.1-01 can be found under [GitHub releases](https://github.com/mrumpf/nexus-pagedecoration-plugin/releases/tag/v2.7.1-01).

For the other plugins I did a fork on GitHub and made some releases on GitHub:
*   [page-markup](https://github.com/mrumpf/page-markup) release: [v0.4-SNAPSHOT](https://github.com/mrumpf/page-markup/releases/tag/v0.4-SNAPSHOT)
*   [sonar-pagedecoration-plugin](https://github.com/fcrespel/sonar-pagedecoration-plugin) release: [v1.0.1-SNAPSHOT](https://github.com/mrumpf/sonar-pagedecoration-plugin/releases/tag/v1.0.1-SNAPSHOT)
*   [nexus-pagedecoration-plugin](https://github.com/mrumpf/nexus-pagedecoration-plugin) release: [v2.7.1-01](https://github.com/mrumpf/nexus-pagedecoration-plugin/releases/tag/v2.7.1-01)

