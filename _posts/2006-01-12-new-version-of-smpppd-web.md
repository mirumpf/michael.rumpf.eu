---
layout: single
title: New version of smpppd-web
date: '2006-01-12T05:46:00.000+01:00'
author: Michael Rumpf
tags:
- linux
- isdn
- suse
modified_time: '2011-04-15T23:55:47.728+02:00'
categories:
---

The original versions of [smppd-web](http://www.suse.de/~cwh/) by Christopher Hofmann neither support adding and removing links on the fly nor do they support dial-on-demand ISDN interfaces. That is the reason why I extended the original codebase by these features.  
Further more the users for which I developed the software are german speaking only so that gettext support was also an important feature. Initially I added  support for german and english languages.  

As I'm not a SuSE user in general, the new version probably does not follow the SuSE directory guidelines and it does not come packaged as a RPM. Therefore it would be great if someone with SuSE RPM packaging know-how would create a successor RPM package out of Christopher's original version. Or even better Christopher packages a new version for [OpenSuSE](http://old-en.opensuse.org/User:Cwh) ;)  

Please see the README in the [tarball](http://michael.rumpfonline.de/projects/smpppd-web.tgz) for more information.  

**Update 20060113**: Christopher Hofmann was so kind to create a [RPM](http://www.suse.de/~cwh/rpms/10.0/) for OpenSuSE 10.0 and promised to feed it into OpenSuSE 10.1: [smpppd-web.spec](https://build.opensuse.org/package/view_file?file=smpppd-web.spec&package=smpppd-web&project=network&srcmd5=29258e9f29b1461712d123aac39c510a) 

````
Fri Jan 13 2006 cwh@suse.de version 3.00
 - now supports adding and removing links on the fly
 - support dial-on-demand interfaces 
 - supports multiple languages with gettext (currently included is german and english)
````