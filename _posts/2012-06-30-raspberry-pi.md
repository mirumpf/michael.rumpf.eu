---
layout: single
title: Raspberry Pi
date: '2012-06-30T12:26:00.001+02:00'
author: Michael Rumpf
tags:
- linux
- raspberrypi
modified_time: '2012-07-17T23:49:27.351+02:00'
categories:
---

My Raspberry Pi finally arrived after 4 months:  

![](http://lh4.ggpht.com/-d4He0twcz6g/T-7UaBINYfI/AAAAAAAAJTQ/2exhKrF50Ck/1341051972105.png)  

The stable Debian distribution "Squeeze" does not contain support for all of the Raspberry Pi 
features. That means there is no way around the the latest Debian "Wheezy" image: 
[http://www.raspberrypi.org/archives/1435](http://www.raspberrypi.org/archives/1435)  

But even with the latest image version there is still some configuration effort left...  

## WLAN Stick

For network connectivity I have chosen the Conrad WLAN Stick N150 Nano, which is based on the following chipset: USB: 0BDA:8176 Realtek Semiconductor Corp. RTL8188CUS 802.11n WLAN Adapter.  
Unfortunately this chipset has some issues with the current drivers from both Debian Linux images as discussed in the forum thread [Automatic setup for RTL8188CUS based wifi adapters](http://www.raspberrypi.org/phpBB3/viewtopic.php?f=26&t=6256&sid=85adf66d944a48285a706d8cb6884190&start=350). By executing the script install-rtl8188cus-latest.sh from the forum thread, the adapter started working immediately after the script was finished.  
The general WLAN configuration is described in detail in the Debian Wiki: [http://wiki.debian.org/WiFi/HowToUse#Command_Line](http://wiki.debian.org/WiFi/HowToUse#Command_Line)  

## Analog Audio

The following commands must be issued to activate the analog output of the Raspberry Pi:  

````
sudo apt-get install alsa-utils gstreamer-tools madplay gstreamer0.10-alsa  
sudo modprobe snd_bcm2835  
sudo amixer cset numid=3 1 
````

Now you can play a mp3 audio stream via the ALSA device like this:  

````
gst-launch souphttpsrc location=http://dradio_mp3_dlf_m.akacast.akamaistream.net/7/249/142684/v1/gnl.akacast.akamaistream.net/dradio_mp3_dlf_m \\  
   ! mad \\  
   ! audioconvert \\
   ! audioresample \\
   ! alsasink device=hw:0
````

The fact that the device needs to be specified explicitly "device=hw:0" indicates another issue here as this is not necessary on my desktop Linux machine.  

For testing the output without an internet connection you can use the following commands:

````
cd /opt/vs/src/hello_pi  
make
./hello_audio
````

The audio output is not as clear as I expected it to be. I still hear some clicking noises when playing a stream. I hope this will be fixed in upcoming releases of the analog audio driver.  

## GPIO Ports

In a first test I connected a button to the GPIO port 0 and tried to read the switch state via a simple script:  

````
sudo -s  
echo "0" > /sys/class/gpio/export  
echo "in" > /sys/class/gpio/gpio0/direction  
watch cat /sys/class/gpio/gpio0/value
````

When pressing and holding the button pressed for at least 2 seconds you will see that the value turns from 1 (not pressed) to 0 (pressed).  

Because the polling approach consumes too much resources I started looking after an interrupt solution.  
The Debian "Wheezy" Beta version does not support interrupts for input ports. However, patches exist and have been included into Linux kernel version 3.2.21 from here: [http://www.bootc.net/archives/2012/07/01/new-revision-of-3-2-21/](http://www.bootc.net/archives/2012/07/01/new-revision-of-3-2-21/)  
In order to download the kernel and activate it, run the following commands (NOTE: the kernel version changes quite frequently, so the 3.2.23 might be outdated):  

````
wget http://apt.bootc.net/debian/pool/main/l/linux-source-3.2.23-rpi1+/linux-image-3.2.23-rpi1+_1_armel.deb  
sudo dpkg -i linux-image-3.2.23-rpi1+_1_armel.deb
sudo cp /boot/vmlinuz-3.2.23-rpi1+ /boot/kernel.img
````

After a reboot the kernel provides interrupts on GPIO input ports and a simple C application published on 
the following thread demonstrates how to handle interrupts: 
[http://www.raspberrypi.org/phpBB3/viewtopic.php?t=7509&p=92074](http://www.raspberrypi.org/phpBB3/viewtopic.php?t=7509&p=92074)  
