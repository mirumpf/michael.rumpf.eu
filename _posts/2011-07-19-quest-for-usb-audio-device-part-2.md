---
layout: single
title: Quest for a USB audio device part 2...
date: '2011-07-19T20:21:00.007+02:00'
author: Michael Rumpf
tags:
- audio
- openwrt
modified_time: '2011-08-12T00:03:45.323+02:00'
categories:
---

The new audio devices arrived but unfortunately they show the same glitches as the devices I tested 
[before](http://dev-snapshot.blogspot.com/2011/07/seeking-cheap-usb-audio-device-that.html"):

1. [USB Soundkarte mit Virtual 3D Soundeffekt LogiLink®]("http://www.logilink.eu/showproduct/UA0053.htm)
This device is also based on the chipset "Tenx Technology, Inc. TP6911 Audio Headset" and has the same 
issues as the previous device (USB Sound Audio), i.e. the output is far too loud.

 2. [USB Soundkarte mit Virtual 7.1 Soundeffekt LogiLink®]("http://www.logilink.eu/showproduct/UA0078.htm)
The sound is good but as with the previous device a clicking noise is heard during reboot, power-off, 
power-on, plug-in/out. The chipset is "C-Media Electronics, Inc. Audio Adapter"
So both adapters use the same chipsets and show the same issues. It seems as if there are not 
too many different cheap USB audio devices on the market, just rebranded stuff from the same 
manufacturer.

... to be continued!
