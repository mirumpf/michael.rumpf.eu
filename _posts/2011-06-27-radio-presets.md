---
layout: single
title: Radio presets
date: '2011-06-27T23:26:00.002+02:00'
author: Michael Rumpf
tags:
- audio
- upnp
- openwrt
modified_time: '2011-07-17T18:33:10.538+02:00'
categories:
---

One important feature for the audio renderer is the support for **presets**.

I do not want to search for my mobile phone in order to select a music source when I enter the kitchen in the morning. The use-case where you turn on your analog radio and the tuning wheel is right at the position of your favorite radio station is unbeatable simple.

Therefore the audio-renderer has a list of MP3 streaming URLs where the first one will be played upon startup of the process. The channels can be switched by sending a signal to the server process:

````
kill -USR1 <PID of audio-renderer>
````

This is a good way to hijack an unused button of your router (for example the QSS button of the TP-Link WR1043ND) and write a simple script as shown here: [http://wiki.openwrt.org/doc/howto/hardware.button](http://wiki.openwrt.org/doc/howto/hardware.button)

There is currently no support for M3U files as this is not supported by Gstreamer. The MP3 streams must be extracted manually from the M3U files.