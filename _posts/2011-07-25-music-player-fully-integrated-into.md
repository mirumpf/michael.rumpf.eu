---
layout: single
title: Music player fully integrated into the OpenWRT build process
date: '2011-07-25T00:44:00.006+02:00'
author: Michael Rumpf
tags:
- audio
- openwrt
- linux
modified_time: '2011-07-25T07:31:58.329+02:00'
categories:
---

The [m3ddity-player](http://www.jcoderz.org/m3dditiez/wiki/m3ddity-player) and the GUPnP libraries, 
on which the player depends upon, can now easily be build for OpenWRT, just follow the steps below:  

1\. Checkout the trunk version of OpenWRT
````
svn co svn://svn.openwrt.org/openwrt/trunk
cd trunk
````

2. Add the [jCoderZ](http://www.jcoderz.org/m3dditiez/) package repository to the list of feeds   
````
cp feeds.conf.default feeds.conf  
echo "src-svn jcoderz " \   
     "http://www.jcoderz.org/svn/m3dditiez/trunk/" \   
"openwrt/packages" >> feeds.conf  
./scripts/feeds update   
./scripts/feeds install -a  
````

3\. Run menuconfig and select the packages as described 
[here](http://dev-snapshot.blogspot.com/2011/07/building-openwrt-image-optimized-for.html)  
```` 
make menuconfig
````

4\. Additionally select the package Multimedia/m3ddity-player. This automatically selects the necessary GUPnP libraries.  

5. Finally, you can build the image  
````
make -j 5
`````

**But be warned, the trunk version of OpenWRT might be broken and flashing the image might result 
in a bricked device that can only be restored via the serial console. For the TL-WR1043ND, this even involves soldering!**