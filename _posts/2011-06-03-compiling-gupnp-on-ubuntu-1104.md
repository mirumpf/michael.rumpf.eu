---
layout: single
title: Compiling GUPnP on Ubuntu 11.04
date: '2011-06-03T15:55:00.002+02:00'
author: Michael Rumpf
tags:
- linux
- upnp
- ubuntu
modified_time: '2011-07-17T18:54:52.725+02:00'
categories:
---

Because the [GUPnP](http://gupnp.org/) stack is lacking some documentation I describe my experiences with getting it complied under Ubuntu 11.04.   
The commands will setup an environment that does not interfere with the Ubuntu provided packages, which are older than the ones compiled here. The library files will be installed under the location: 
~/workspaces/openwrt/upnp  
  
  
**Download:**
````
mkdir -p ~/workspaces/openwrt/upnp  
cd ~/workspaces/openwrt/upnp  
wget -O - http://gupnp.org/sites/all/files/sources/gupnp-igd-0.1.11.tar.gz | tar xvfz -  
wget -O - http://gupnp.org/sites/all/files/sources/gupnp-0.16.1.tar.gz | tar xvfz -  
wget -O - http://gupnp.org/sites/all/files/sources/gupnp-igd-0.1.10.tar.gz | tar xvfz -  
wget -O - http://gupnp.org/sites/all/files/sources/gupnp-tools-0.8.3.tar.gz | tar xvfz -  
wget -O - http://gupnp.org/sites/all/files/sources/gupnp-dlna-0.6.1.tar.gz | tar xvfz -  
wget -O - http://gupnp.org/sites/all/files/sources/gupnp-av-0.8.0.tar.gz | tar xvfz -  
wget -O - http://gupnp.org/sites/all/files/sources/gssdp-0.10.0.tar.gz | tar xvfz -
wget -O - http://gupnp.org/sites/all/files/sources/gupnp-vala-0.8.0.tar.gz | tar xvfz -
`````

**Prerequisites:**  
````
$ sudo apt-get install gstreamer0.10-dev uuid-dev libsoup2.4-dev libxml2-dev libgstreamer-plugins-base0.10-dev gtk+3.0 libgtk-3-dev valac-0.12 libgee-dev libgconf2-dev  
`````

**Compilation:**
````
cd gssdp-0.10.0
./configure --prefix ~/workspaces/openwrt/upnp --exec-prefix ~/workspaces/openwrt/upnp  
gssdp-0.10.0$ make && make install  
cd ../gupnp-0.16.1  
gupnp-0.16.1$ PKG_CONFIG_PATH=~/workspaces/openwrt/upnp/gssdp-0.10.0 ./configure --prefix ~/workspaces/openwrt/upnp --exec-prefix ~/workspaces/openwrt/upnp  
make && make install  
cd ../gupnp-av-0.8.0  
PKG_CONFIG_PATH=~/workspaces/openwrt/upnp/gupnp-0.16.1 ./configure --prefix ~/workspaces/openwrt/upnp --exec-prefix ~/workspaces/openwrt/upnp  
gupnp-av-0.8.0$ make && make install  
cd ../gupnp-dlna-0.6.1  
./configure --prefix ~/workspaces/openwrt/upnp --exec-prefix ~/workspaces/openwrt/upnp  
make && make install  
cd ../gupnp-igd-0.1.11  
PKG_CONFIG_PATH=~/workspaces/openwrt/upnp/gupnp-0.16.1 ./configure --prefix ~/workspaces/openwrt/upnp --exec-prefix ~/workspaces/openwrt/upnp
make && make install
cd ../gupnp-tools-0.8.3
PKG_CONFIG_PATH=~/workspaces/openwrt/upnp/gupnp-0.16.1 ./configure --prefix ~/workspaces/openwrt/upnp --exec-prefix ~/workspaces/openwrt/upnp
make && make install
cd ../gupnp-vala-0.8.3
PKG_CONFIG_PATH=~/workspaces/openwrt/upnp/gssdp-0.10.0 ./configure --prefix ~/workspaces/openwrt/upnp --exec-prefix ~/workspaces/openwrt/upnp  
make && make install
````

The gupnp-media-renderer (version 0.1) is out of scope here as it is not maintained anymore. The code has been integrated into [Rygel](http://live.gnome.org/Rygel).