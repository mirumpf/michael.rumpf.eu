---
layout: single
title: Creating an ISDN Gateway with SuSE 9.3
date: '2005-10-19T07:31:00.000+02:00'
author: Michael Rumpf
tags:
- linux
- isdn
- suse
modified_time: '2011-06-12T13:24:01.719+02:00'
categories:
---

In Germany flatrates are available for DSL only, so people, living in areas where DSL is not available, have to pay expensive time based fees for slowly connecting to the Internet. The problem is that M$ Windows and many of the installed tools are known for initiating internet connections although the user did not ask them to do so. Therefore it is crucial to reliably disconnect the ISDN line so that those unwanted connection attempts will not succeed. ISDN routers, which might be of help here, are hard to find nowadays and do not offer the comfort as new DSL routers do.  

A solution should fulfill the following additional requirements:  

*   provide secure internet access
*   provide a secure way to do remote administration
*   provide centralized storage
*   provide a Wiki for documentation
*   should also be usable as a client machine

After doing some investigation I found the following possible solutions:  

1.  Tools on a Windows server
2.  Linux Diskless Router based solution
3.  Scripts from a Windows client executed on the Linux Server
4.  Linux server with a web based approach

The first category solutions, for example [Internet Remote Control](http://www.bysoft.com/internet_remote_control.php), fall short because the Windows machine is connected directly to the Internet and thus vulnerable to all kind of attacks (Who really trusts the Windows XP firewall?).  

With software from the second class it is not possible to provide centralized storage via [Samba](http://www.samba.org/). I know that this is by far the most secure way but it would mean booting another machine with no other purpose than Internet access.  

The third and fourth solution can be deployed together on one Linux server machine, which can also be used as a desktop. This machine provides secure Internet access via ISDN and at the same time centralized storage to the Windows clients on the network. The script based solution has one deficiency, it does not allow continous feedback on the connection state of the ISDN interface. Therefore the last solution is by far the one with the greatest flexibility and has been chosen to build a final solution.  

The machine has been installed with SuSE Linux 9.3 as a desktop machine. The following services have been enabled:

*   Apache2
*   Samba
*   SSH

The `ddclient` package is used to update dyndns.org records when the ISDN connection is established. This allows easy remote administration, as the IP address gets updated automatically.  

As a little gimmick I added the script `xsend.py` from the [xmpppy](http://xmpppy.sourceforge.net/) project which sends a message with the ippp0 configuration to my Jabber account so that I get notified when the machine is connected to the Internet.  

Both scripts `xsend.py` and `ddclient` are executed when the connection is established. This is done by the script `/etc/ppp/ip-up.local` which gets called by the ISDN stack upon connection establishment.  

The excellent tool smpppd and smpppd-web is used to manage the ISDN connections.