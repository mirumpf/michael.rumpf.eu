---
layout: single
title: Seeking a cheap USB audio device for the TP-Link TL-WR1043ND
date: '2011-07-13T22:48:00.004+02:00'
author: Michael Rumpf
tags:
- audio
- openwrt
modified_time: '2011-07-17T18:57:19.731+02:00'
categories:
---

I'm seeking for a USB audio device that works with the TP-Link TL-WR1043ND under OpenWRT. Until now I tried 2 different devices with unacceptable issues.  

**1. USB Sound Audio (Amazon)**  

[![](http://2.bp.blogspot.com/-qWqnVo0dbP8/Th4AOAsdarI/AAAAAAAAElw/m7Hwk4flU3U/s200/TP6911.jpg)](http://2.bp.blogspot.com/-qWqnVo0dbP8/Th4AOAsdarI/AAAAAAAAElw/m7Hwk4flU3U/s1600/TP6911.jpg)

The device gets detected under Linux as Tenx Technology, Inc. TP6911 Audio Headset. The following warnings are reported by 

dmesg when connecting the device:  

````
usb_audio: Warning! Unlikely big volume range (=13568), cval->res is probably wrong.  
usb_audio: [2] FU [PCM Playback Volume] ch = 2, val = 2560/16128/1  
usb_audio: Warning! Unlikely big volume range (=6096), cval->res is probably wrong.  
usb_audio: [5] FU [Mic Capture Volume] ch = 1, val = 0/6096/1  
````

The issue with this device is that the volume level is so high that it cannot be controlled. Between mute and 10% the output sounds distorted. At 20% the volume level is at its maximum, with no audible change between 20% and 100%.
**2. auvisio Visual Sound Technologies (pearl.de)**

[![](http://1.bp.blogspot.com/-7uen3q5pdPQ/Th4AVGQtxQI/AAAAAAAAEl0/vizbd1NDDCU/s200/auvisio-virtual-7-1-usb.gif)](http://1.bp.blogspot.com/-7uen3q5pdPQ/Th4AVGQtxQI/AAAAAAAAEl0/vizbd1NDDCU/s1600/auvisio-virtual-7-1-usb.gif)

The device gets detected as C-Media Electronics, Inc. Audio Adapter.

The issue with the device is that a very loud clicking sound gets played when the OpenWRT router starts-up or during shutdown/power-off. The sound is quite loud and it will be very annoying when using the router as a music-player.

**Next**

Both devices are unsuitable for using them with the router. The next try will be with the following devices...

* USB Soundkarte mit Virtual 7.1 Soundeffekt LogiLink®](http://www.logilink.eu/showproduct/UA0078.htm)
* USB Soundkarte mit Virtual 3D Soundeffekt LogiLink®](http://www.logilink.eu/showproduct/UA0053.htm)
