---
layout: single
title: m3player version 0.1.2 released
date: '2011-08-01T01:22:00.001+02:00'
author: Michael Rumpf
tags:
- audio
- openwrt
modified_time: '2011-08-01T01:33:36.523+02:00'
categories:
---

I just released version 0.1.2 of [m3player](http://www.jcoderz.org/m3dditiez/wiki/m3ddity-player). The new version includes:  

*   Compilation and packaging for OpenWRT
*   Installation of the ipkg package on the TL-WR1043ND device
*   Support for command-line parameters:
    * config - Path to the config file (<span style="font-family: &quot;Courier New&quot;,Courier,monospace;">/etc/m3player/m3player.ini</span>)
    * pidfile - Location of the PID file (<span style="font-family: &quot;Courier New&quot;,Courier,monospace;">/var/run/m3player.pid</span>)
    * xmlfolder - Location of the descriptor XML files (<span style="font-family: &quot;Courier New&quot;,Courier,monospace;">/usr/share/m3player</span>)
    * name - Set the name of the player instance (Hostname or m3player)
    * filename - The name of the root device file (<span style="font-family: &quot;Courier New&quot;,Courier,monospace;">MediaRenderedV1.xml</span>)
*   Support for an INI file (<span style="font-family: &quot;Courier New&quot;,Courier,monospace;">/etc/m3player/m3player.ini</span>)
*   Fixes to the GStreamer pipeline to work in the stripped down OpenWRT environment with only a few plugins

When launching the [m3player](http://www.jcoderz.org/m3dditiez/wiki/m3ddity-player) it starts playing the first entry from the preset list.  

The next version will include support for preset switching via the QSS button.