---
layout: single
title: Intercepting Win32 API system calls
date: '2005-07-03T01:04:00.000+02:00'
author: Michael Rumpf
tags:
- windows
- reverse engineering
modified_time: '2011-06-13T21:41:45.695+02:00'
categories:
---

For reverse-engineering the Windows software of the _TFA Klima-Logger_ I tried to analyze the I2C protocol over the serial line by intercepting Win32 API system calls.  

Searching with Google about the topic _intercept Win32 function_ I stumbled over the Microsoft Research tool [Detours](http://research.microsoft.com/sn/detours/) which was exactly what I was looking for. Version 1.5 of the tool is freely available for download and already comes with an example to intercept serial API system calls which served as a starting point for my serial communication interception library.  

Here is how it works: The Detours library provides means to copy the first few bytes from the actual library function text segment to the beginning of the interception library function. It copies as many bytes as necessary to write an unconditional jump to the beginning of the original Win32 function instead. This unconditional jump just moves the execution pointer to the interception function which executes the bytes just copied, before it executes the actual interception code. Finally it jumps to the original function entry point, skipping the bytes of the unconditional jump and performs the normal library function's tasks. A precise description of the Detours concept can be found [here](http://research.microsoft.com/pubs/68568/huntusenixnt99.pdf).  

In order to get the library into the address space of the application whose Win32 function calls should be intercepted, Detours delivers a small tool with which the interception library can be injected into the process to be analyzed. Once injected the interception mechanisms are installed and each Win32 function call is routed through the function provided by the Detours library. 

See the [i2canalyzer](http://michael.rumpfonline.de/projects/i2canalyzer.tar.gz) example on how to intercept serial communication functions. This is an extended version of the Detours example _traceser_ with additional functionality of logging information to different files.  

The _i2canalyzer_ example might be extended when using the Linux I2C layer approach for communicating with the device does not lead to the expected results.  

One of the biggest problems on getting the Detours examples running was the missing compile environment on Windows. If you are interested in how this can be achieved, have a look at the article on _Setting up a Microsoft C++ compile environment at no charge on Windows XP_.