---
layout: single
title: Building a TP-Link TL-WR1043ND OpenWRT 10.03 image
date: '2011-07-31T23:48:00.007+02:00'
author: Michael Rumpf
tags:
- linux
- openwrt
modified_time: '2011-08-09T10:37:43.302+02:00'
categories:
---

This article gives a step-by-step description on how to get the 
[m3ddity-player](http://www.jcoderz.org/m3dditiez/wiki/m3ddity-player) working on the 
TP-Link TL-WR1043ND device, using the stable version of OpenWRT 10.03 (backfire).  

First you need to checkout the OpenWRT 10.03 (backfire) branch:  
````
svn co svn://svn.openwrt.org/openwrt/branches/backfire
cd backfire
````

Then the feed for the [m3ddity-player](http://www.jcoderz.org/m3dditiez/wiki/m3ddity-player) must be added:  

````
cp feeds.conf.default feeds.conf
echo "src-svn jcoderz http://www.jcoderz.org/svn/m3dditiez/branches/openwrt-10.3/packages" >> feeds.conf
./scripts/feeds update ./scripts/feeds install -a
````

Now run menuconfig and select the necessary modules:  
````
make menuconfig 
````

The following list shows only menuconfig entries where settings have been changed. Settings not shown here are left untouched.  

````
Target System (Atheros AR71xx/AR7240/AR913x)  
Target Profile (TP-Link TL-WR1043ND v1)  

Global build settings --->  
  [*] Remove ipkg/opkg status/data files in final images  
  [ ] Enable ipv6 support in packages  

[*] Build the OpenWRT based toolchain  
[*] Image configuration --->  
    (192.168.0.5)LAN DNS server  
    (192.168.0.5)LAN Gateway 
    (255.255.255.0) LAN Network Mask  
    (192.168.0.111) LAN IP Address 
  
Base system  
  <*> block-mount 
  < > dnsmasq 
  < > firewall 

Kernel Modules
  Filesystems  
    <*> kmod-fs-ext3 (for USB drive)  
    <*> kmod-nls-utf8 

  Other modules 
    <*> kmod-ledtrig-netdev 

  Sound Support  
    <*> kmod-sound-core  

  USB Support  
    <*> kmod-usb-core  
    <*>   kmod-usb-audio  
    <*> kmod-usb-ohci  
    <*> kmod-usb-storage (for USB drive)  

Network
  < > ppp
  < > iptables 

Time Synchronization<*>ntpdate

Multimedia 
  <*> gst-mod-alsa
  <*> gst-mod-mad
  <*> gst-mod-souphttpsrc
  <*> gstreamer
  <*> m3ddity-player (only with [jCoderZ.org](http://www.jcoderz.org/m3dditiez/wiki/openwrt-packages) modules)

Utilities
  <*> alsa-utils (for alsamixer/alsactl)
````

After the configuration step is completed, the image can be compiled with the following command:  
make
  
The next steps assume that the router is already running a stable version of OpenWRT (flashed initially via the original vendor UI). If that is the case you can now connect to the router by running:  
````
telnet 192.168.0.111 # The IP configured during menuconfig 
````

Now copy the new image to a location on the router with enough space, for example a USB stick (/dev/sda1):  
````
mkdir /mnt/usb 
mount -t ext2 /dev/sda1 /mnt/usb 
cd /mnt/usb 
scp USERNAME@HOST:HOME/backfire/bin/ar71xx/openwrt-ar71xx-generic-tl-wr1043nd-v1-squashfs-sysupgrade.bin . 
````

Now drop the caches:  
````
echo 3 > /proc/sys/vm/drop_caches 
````

Then reflash the image with the following command:  
````
mtd -r write openwrt-ar71xx-generic-tl-wr1043nd-v1-squashfs-sysupgrade.bin firmware 
````

The router will reboot and you can connect again after reboot:  
````
telnet 192.168.0.111 
````

Replace the USB stick with the USB audio device and run the m3ddity-player with the following command:  
````
m3player 
````

Now you should hear a sound as the m3player starts playing one of its presets from the config file /etc/m3player/m3player.ini.
