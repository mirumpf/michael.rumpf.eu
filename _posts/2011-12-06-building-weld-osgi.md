---
layout: single
title: Building weld-osgi
date: '2011-12-06T23:11:00.001+01:00'
author: Michael Rumpf
tags:
- osgi
- maven
- java
modified_time: '2012-07-17T23:45:34.912+02:00'
categories:
---

In case you want to use [Weld](http://seamframework.org/Weld) inside your OSGi application you first need to compile a few branches on your own:  

*   [WELD-963](https://github.com/mathieuancelin/api/tree/WELD-963) branch of the weld/api project
*   [weld-osgi](https://github.com/mathieuancelin/core/tree/weld-osgi) branch from the weld/core project

Those branches will soon be merged into the official Weld codebase. But in the meantime you need to follow the steps below in order to get the packages compiled.  
A prerequisite is that git and Maven 3 is available  

````
git clone -b WELD-963 http://github.com/mathieuancelin/api.git WELD-963
cd WELD-963
mvn clean install
cd ..
git clone -b weld-osgi http://github.com/mathieuancelin/core.git weld-osgi
cd weld-osgi
mvn clean install -Dmaven.test.skip=true
find . -name "weld-osgi-core*SNAPSHOT.jar" -exec cp {} . \;
````

The last command above copies the following files into your current directory:

*   weld-osgi-core-api-1.1.5-SNAPSHOT.jar
*   weld-osgi-core-extension-1.1.5-SNAPSHOT.jar
*   weld-osgi-core-integration-1.1.5-SNAPSHOT.jar
*   weld-osgi-core-mandatory-1.1.5-SNAPSHOT.jar
*   weld-osgi-core-spi-1.1.5-SNAPSHOT.jar

You can now deploy those packages into your local repository so that they can be used in your own project's pom.xml.

````
mvn deploy:deploy-file -DgroupId=org.jboss.weld.osgi \
-DartifactId=weld-osgi-core-api \
-Dversion=1.1.5-SNAPSHOT \
-Dpackaging=jar \
-Dfile=weld-osgi-core-api-1.1.5-SNAPSHOT.jar
````

````
mvn install:install-file -DgroupId=org.jboss.weld.osgi  \
-DartifactId=weld-osgi-core-extension  \
-Dversion=1.1.5-SNAPSHOT \
-Dpackaging=jar \
-Dfile=weld-osgi-core-extension-1.1.5-SNAPSHOT.jar  
````

````
mvn install:install-file -DgroupId=org.jboss.weld.osgi  \
-DartifactId=weld-osgi-core-integration  \
-Dversion=1.1.5-SNAPSHOT \
-Dpackaging=jar \
-Dfile=weld-osgi-core-integration-1.1.5-SNAPSHOT.jar
````

````
mvn install:install-file -DgroupId=org.jboss.weld.osgi  \
                         -DartifactId=weld-osgi-core-mandatory  \
                         -Dversion=1.1.5-SNAPSHOT \
                         -Dpackaging=jar \
                         -Dfile=weld-osgi-core-mandatory-1.1.5-SNAPSHOT.jar
````

````
mvn install:install-file -DgroupId=org.jboss.weld.osgi  \
                         -DartifactId=weld-osgi-core-spi  \
                         -Dversion=1.1.5-SNAPSHOT \
                         -Dpackaging=jar \
                         -Dfile=weld-osgi-core-spi-1.1.5-SNAPSHOT.jar
````

The pom.xml of your assembly package might look like this:  

````
        <dependency>  
            <groupId>org.jboss.weld.osgi</groupId>  
            <artifactId>weld-osgi-core-api</artifactId>  
            <version>1.1.5-SNAPSHOT</version>  
            <scope>runtime</scope>  
        </dependency>  
        <dependency>  
            <groupId>org.jboss.weld.osgi</groupId>  
            <artifactId>weld-osgi-core-extension</artifactId>  
            <version>1.1.5-SNAPSHOT</version>  
            <scope>runtime</scope>  
        </dependency>  
        <dependency>  
            <groupId>org.jboss.weld.osgi</groupId>  
            <artifactId>weld-osgi-core-integration</artifactId>  
            <version>1.1.5-SNAPSHOT</version>  
            <scope>runtime</scope>  
        </dependency>  
        <dependency>  
            <groupId>org.jboss.weld.osgi</groupId>  
            <artifactId>weld-osgi-core-mandatory</artifactId>  
            <version>1.1.5-SNAPSHOT</version>  
            <scope>runtime</scope>  
        </dependency>  
        <dependency>  
            <groupId>org.jboss.weld.osgi</groupId>  
            <artifactId>weld-osgi-core-spi</artifactId>  
            <version>1.1.5-SNAPSHOT</version>  
            <scope>runtime</scope>  
        </dependency>  
````
