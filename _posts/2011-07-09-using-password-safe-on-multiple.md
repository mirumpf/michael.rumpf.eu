---
layout: single
title: Using a password safe on multiple platforms
date: '2011-07-09T15:40:00.005+02:00'
author: Michael Rumpf
tags:
- keepass
- dropbox
- security
modified_time: '2011-07-17T18:32:55.355+02:00'
categories:
---

After the Playstation Network Hack I decided to change to secure passwords for all my accounts. 
The drawback is that nobody can remember secure passwords anymore, so there is no way around using a 
password safe.  

The only application that is available on all the different platforms I use is KeePass (V1.x on Windows, 
KeePassX under Linux, and KeePassDroid on Android). Under Windows the the 1.x version must be used in 
order to stick to the same file format as the KeePassX and the KeePassDroid applications.  

Using one database file on many different devices has proven to be a challenge as the file must be 
located where it can be accessed from all the different devices.  

My first approach was to use a WebDAV folder which is available from hosters like web.de or 1&1 in 
Germany.  
The Linux support for WebDAV is good, dav2fs does what I was looking for and I found absolutely no 
issues while using it.  
Unfortunately Windows 7 64Bit support for WebDAV is severly broken. Access to the online folder was 
working a few days but it stopped working, being unable to connect anymore (Error 0x80070043). All 
the hints and workarounds I found did not work for my business notebook. I also tried the different 
WebDAV client tools like [NetDrive](http://www.netdrive.net/) for example. None of them was working 
reliably in a corporate environment behind a proxy and I did not want to find workarounds for the 
issues of the WebDAV clients.  
There is also no free WebDAV support available on Android, making the protocol unusable on my phone 
and tablet. The consequence would be either to copy the file manually or to extend KeePassDroid in 
order to access databases via HTTPS/WebDAV.  

After a few weeks of pain when trying to log into an account with a secure password I decided to 
give DropBox a try. Clients are available for all the devices I have and adding an account is very 
easy and can be setup in under a minute.  

DropBox has been configured to create a new folder and not to sync the whole user's home folder:  

*   Windows: C:\Users\(USERNAME)<username>\Dropbox</username>
*   Linux: /home/(USERNAME)<username>/Dropbox</username>
*   Android: /mnt/sdcard/dropbox

On all platforms the extension "kdb" has been associated with the KeePass application, so opening the 
DropBox folder and double-clicking/pressing on the kdb file opens KeePass/KeePassX/KeePassDroid which 
asks for the database password.  

So far DropBox seems to be the solution to my problem. I can access the database with only 2 
passwords to remember, the one for the password safe file and the other one for the DropBox account.  

However, one issue with concurrent access to the same KeePass file remains. The KeePass application 
writes a lock file to signal that the database is being used by an instance of the application. 
Different instances create and delete the lock file so that the file is completely useless in the 
scenario, described above. The message that a lock file has been found and whether the database 
should be opened read-only must either be ignored or followed. When followed new entries can be 
added on one device only.  

When you add an entry to the database on one platform this change is propagated to the other 
devices because of the DropBox sync. Unfortunately running instancs of KeePass do not recognize 
that a file has changed in the DropBox folder. Only when you add an account there and you try to 
save the database file you get a message that the file in the file-system has changed.  
The  

The solution to this issue would be that KeePass reloads the file on a regular basis as long as no 
entries have been added. In case a user has added an entry KeePass could do a merge of the two 
database files by comparing entries one by one and adding new ones to the loaded database before 
persisting it again.  

As long as this has not been implemented the only work-around is to do some configuration on the 
clients so that the application/database is likely to be closed each time you switch the device.  
One setting is not minimize the application window into the system tray. That keeps the window 
open and reminds you, when you, for example, cleanup the desktop of your business notebook in 
evening, to close the application. The next morning you need to reopen the database and load all 
entries that have been added last evening on your other devices.  

A small step into the merge direction would be to notify the user when the database in the 
file-system has changed. If such a notification pops-up the database can be closed and re-opened 
again.  

I have created feature requests for each of the three projects:  

*   KeePass: [3362189](https://sourceforge.net/tracker/?func=detail&aid=3362189&group_id=95013&atid=609908) (Closed, Rejected:[http://keepass.info/help/base/multiuser.html](http://keepass.info/help/base/multiuser.html))
*   KeePassX: [3362184](https://sourceforge.net/tracker/?func=detail&aid=3362184&group_id=166631&atid=839782)
*   KeePassDroid: [206](http://code.google.com/p/keepassdroid/issues/detail?id=206)