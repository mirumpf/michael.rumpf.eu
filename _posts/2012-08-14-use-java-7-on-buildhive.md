---
layout: single
title: Use Java 7 on Buildhive
date: '2012-08-14T14:09:00.000+02:00'
author: Michael Rumpf
tags:
- java
- maven
modified_time: '2012-08-14T14:10:31.434+02:00'
categories:
---

Changing source and target version to 1.7 is not sufficient to enable Java 7 on Buildhive at the moment.  
Adding the following profile to your Maven POM makes sure that your local build is not affected by 
the special configuration and when running on Buildhive, JDK 7 is used for both build and test-run.  

````
  <profiles>  
    <profile>  
      <id>buildhive</id>  
      <build>  
        <plugins>  
          <plugin>  
            <groupId>org.apache.maven.plugins</groupId>  
            <artifactId>maven-compiler-plugin</artifactId>  
            <version>2.5.1</version>  
            <configuration>  
              <source>1.7</source>  
              <target>1.7</target>  
              <showDeprecation>true</showDeprecation>  
              <showWarnings>true</showWarnings>  
              <encoding>UTF-8</encoding>  
              <executable>/opt/jdk/jdk1.7.0/bin/javac</executable>  
              <fork>true</fork>  
            </configuration>  
          </plugin>  
          <plugin>  
            <groupId>org.apache.maven.plugins</groupId>  
            <artifactId>maven-surefire-plugin</artifactId>  
            <configuration>  
              <jvm>/opt/jdk/jdk1.7.0/bin/java</jvm>  
              <forkMode>once</forkMode>  
            </configuration>  
          </plugin>  
        </plugins>  
      </build>  
    </profile>  
  </profiles>  
````

The profile can be selected by adding the following line to the configuration of your Buildhive job:

Maven Goals: `-Pbuildhive clean install`