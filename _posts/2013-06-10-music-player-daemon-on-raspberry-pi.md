---
layout: single
title: Music Player Daemon on Raspberry Pi
date: '2013-06-10T21:06:00.000+02:00'
author: Michael Rumpf
tags:
- audio
- raspberrypi
modified_time: '2013-06-10T21:16:13.985+02:00'
categories:
---

This is not yet another "how to configure [MPD](http://www.musicpd.org/) on the Raspberry Pi" article, 
because there are a lot of excellent [MPD](http://www.musicpd.org/) HOWTOs already available:  

*   [http://hempeldesigngroup.com/embedded/stories/raspberry-pi-setup-as-mpd-sever/](http://hempeldesigngroup.com/embedded/stories/raspberry-pi-setup-as-mpd-sever/)
*   [http://www.t3node.com/blog/streaming-audio-with-mpd-and-icecast2-on-raspberry-pi/](http://www.t3node.com/blog/streaming-audio-with-mpd-and-icecast2-on-raspberry-pi/)
*   [http://lesbonscomptes.com/pages/raspmpd.html](http://lesbonscomptes.com/pages/raspmpd.html)

However, I experienced a bottleneck with the configuration described by those articles. The load 
was slightly over 1 but it was high enough to cause some stuttering every 30 seconds.  
    
The first optimization is to avoid using a separate streaming solution like icecast

MPD has its own, built-in streaming plugin which can be configured like this:  
````
audio_output {   
  type "httpd"   
  name "My HTTP Stream"  
  encoder "flac"  
  port "8000"  
  bitrate "96"  
  format "44100:16:2"  
}  
````

But before adding this you need to check your sound card default output parameters:  
````
sudo cat /proc/asound/card0/pcm0p/sub0/hw_params  
````

which returns something like this:  
````
access: RW_INTERLEAVED   
format: S16_LE   
subformat: STD   
channels: **2**   
rate: **44100** (44100/1)   
period_size: 5513   
buffer_size: 22050  
````

Then note the rate and channels above and make sure they are the same as in mpd.conf:  
````
audio_output {  
  type "alsa"   
  name "My ALSA Device"   
  device "hw:0,0" # optional   
  format "44100:16:2" # CHECK rate and channels
  mixer_device "default" # optional   
  mixer_control "PCM" # optional   
  mixer_index "0" # optional   
}  
    
audio_output {  
  type "httpd"   
  name "My HTTP Stream"  
  # we are using a faster encoder note that some player might not supprt Flac  
  encoder "flac"
  port "8000"  
  # quality "5.0"
  # do not define if bitrate is defined  
  # leave at 128 and check cpu usage...
  bitrate "96"
  # do not define if quality is defined  
  format "44100:16:2"  
}  
    
# This setting will change all decoded audio to be converted to the specified   
# format before being passed to the audio outputs. By default, this setting is   
# disabled.  
#  
audio_output_format "44100:16:2"
#  
# If MPD has been compiled with libsamplerate support, this setting specifies   
# the sample rate converter to use. Possible values can be found in the   
# mpd.conf man page or the libsamplerate documentation. By default, this is   
# setting is disabled.   
#  
samplerate_converter "Fastest Sinc Interpolator"
````

The important hints for those optimizations came from the Raspberry Pi Forum
[here](http://www.raspberrypi.org/phpBB3/viewtopic.php?t=43670&p=350142).</div>