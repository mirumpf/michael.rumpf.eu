---
layout: single
title: VirtualBox 4.0.2 on Ubuntu 11.04
date: '2011-05-29T13:55:00.001+02:00'
author: Michael Rumpf
tags:
- linux
- virtual machine
modified_time: '2011-07-17T18:34:42.177+02:00'
categories:
---

After an update to Ubuntu 11.04 VirtualBox was not running anymore. Startup fails with a hint that the kernel modules exited in error and that I should use DKMS to reconfigure the kernel module.
  
Running the vboxdrv setup command also led to an error: 
  
```` 
$ sudo /etc/init.d/vboxdrv setup 
[sudo] password for desktop:  
 * Stopping VirtualBox kernel modules                                  *  done. 
 * Uninstalling old VirtualBox DKMS kernel modules                     *  done. 
 * Trying to register the VirtualBox kernel modules using DKMS                   
Error! Bad return status for module build on kernel: 2.6.38-8-generic (x86_64) 
Consult the make.log in the build directory 
/var/lib/dkms/vboxhost/4.0.2/build/ for more information. 
 * Failed, trying without DKMS 
 * Recompiling VirtualBox kernel modules                                         
 * Look at /var/log/vbox-install.log to find out what went wrong 
````

The log file indicates that a header file was missing:

````
less /var/lib/dkms/vboxhost/4.0.2/build/make.log 
... 
fatal error: linux/autoconf.h: No such file or directory 
... 
````

The following link led me to the solution (workaround): 
[http://forums.virtualbox.org/viewtopic.php?f=7&t=38584](http://forums.virtualbox.org/viewtopic.php?f=7&t=38584) 

````
... 
ln -s /usr/src/linux-headers-`uname -r`/include/generated/autoconf.h /usr/src/linux-headers-`uname -r`/include/linux/autoconf.h 
... 
````

After the link has been created, the vboxdrv setup command just ran fine: 

````
micha@micha-desktop:~$ sudo /etc/init.d/vboxdrv setup 
 * Stopping VirtualBox kernel modules                                  *  done. 
 * Uninstalling old VirtualBox DKMS kernel modules                     *  done. 
 * Trying to register the VirtualBox kernel modules using DKMS         *  done. 
 * Starting VirtualBox kernel modules                                  *  done.
````