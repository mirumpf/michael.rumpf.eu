---
layout: single
title: Building a custom OpenWRT image for the TP-Link TL-WR1043ND
date: '2011-06-12T16:36:00.001+02:00'
author: Michael Rumpf
tags:
- audio
- openwrt
modified_time: '2011-07-17T18:33:54.433+02:00'
categories:
---

[![](http://1.bp.blogspot.com/-My4aqk4nBL4/TfSaA1akJ5I/AAAAAAAAEkg/GpWbxKqabLs/s200/IMAG0292.jpg)]
[![](http://3.bp.blogspot.com/-eqzxpdUiMJ8/TfSaC26_9QI/AAAAAAAAEkk/5lGKpLAiEvs/s200/IMAG0293.jpg)]

For my low-budget wireless music player I had to tweak OpenWRT in order to support my setup. For initial tests I decided to use the [Music Player Daemon](http://mpd.wikia.com/) and [madplay](http://www.underbit.com/products/mad/).  

The following steps describe how to setup OpenWRT on the TP-Link WR1043ND. It assumes that the OpenWRT 10.03.1-rc4 firmware has already been installed, that the IP address of the router has been set to 192.168.0.111, and that an USB audio, as well as that an USB stick has been connected to the device via an USB hub.  

Checkout the 10.03 branch (currently 10.03.1-rc5)  
````
$ svn co svn://svn.openwrt.org/openwrt/branches/backfire
$ cd backfire
````

Download and install all packages:  
````
$ ./scripts/feeds update -a
$ ./scripts/feeds install -a
````

Run menuconfig and select a few more items so that they are available without the need to install packages first:  
````
$ make menuconfig
-> Target Profile: TP-Link TL-WR1043ND  
-> [*] Build the OpenWrt Image Builder  
-> [*] Build the OpenWrt SDK  
-> [*] Build the OpenWrt based Toolchain  
-> [*] Image Configuration  
     () LAN Gateway:<gw-address></gw-address>  
     (192.168.1.1) LAN Network Address: 192.168.0.111  
     Exit  
-> Base System  
     <*> block-hotplug  
     <*> block-extroot  
     Exit  
-> Kernel Modules<span class="Apple-tab-span" style="white-space: pre;">
     -> Filesystems  
         <*> kmod-fs-ext2  
         Exit  
     -> USB Support  
         <*> kmod-usb-core  
         <*>   kmod-usb-audio  
         <*>   kmod-usb-ohci  
         <*>   kmod-usb-storage  
         <*>   kmod-usb-storage-extras  
         <*>   kmod-usb-usb2  
         Exit  
-> Sound  
      <m>mpd</m>  
      <m>mpc</m>  
      <m>madplay</m>  
     Exit  
Exit  
````

On quad core machines you can run make with 5 parallel jobs:  
````
$ make -j 5
````

Install the new image by running the following commands:  
````
$ telnet 192.168.0.111
$ scp USER<user>@HOST<host>:HOME<home>/workspaces/openwrt/backfire/bin/ar71xx/openwrt-ar71xx-tl-wr1043nd-v1-squashfs-sysupgrade.bin .</home></host></user>
$ cd /tmp
$ mtd -r write openwrt-ar71xx-tl-wr1043nd-v1-squashfs-sysupgrade.bin firmware
````

Because WLAN will not be configured after flashing a new image, you need a cable connection.  
After the reboot is finished you can connect to the device like this:  
````
$ telnet 192.168.0.111
````

Because mpd and madplay consume more space than the router has available on its root partition, the extroot feature must be enabled. Enable extroot for the USB stick by modifiying fstab:  
````
$ vi /etc/config/fstab
================================
config mount
        option target   /overlay
        option device   <span class="Apple-style-span" style="color: red;">**/dev/sda1**
        option fstype   <span class="Apple-style-span" style="color: red;">**ext2**
        option options  rw,sync
        option enabled  <span class="Apple-style-span" style="color: red;">**1**
        option enabled_fsck 0
================================
````

Reboot the device again so that the extroot overlay becomes available:
````
$ reboot
````

Connect to the device again:  
````
$ telnet 192.168.0.111
````

I have setup an Apache on my desktop machine in order to easily provide the packages to the device. For using the local HTTP server, opkg must be configured like this (my desktop machine has IP 192.168.0.17):  
````
$ vi /etc/opkg.conf
================================
src/gz packages http://192.168.0.17/packages
...
================================
````

When this is done you can update the package list:
````
$ opkg update
````
Now the packages mpd, mpc, and madplay can be installed:  
````
$ opkg install mpd mpc madplay
````
The mpd daemon needs some special folder setup:  
````
$ mkdir -p ~/.mpd/playlists
````
Make sure mpd starts when the device boots up by adding the follownig line to rc.local:  
````
$ vi /etc/rc.local
================================

mpd /etc/mpd.conf

================================
````
In oder to disconnect the wired LAN connection, you need to setup WLAN:  
````
$ vi /etc/config/wireless
````
Change the following entries:  
````
================================

config wifi-device  radio0
...
# REMOVE THIS LINE TO ENABLE WIFI:

**#option disabled 1**
  

config wifi-iface
...
**option mode sta # Client mode**
**option ssid mySSID # Your SSID**
**option encryption psk2 # WPA-PSK2**
**option key 12345678901234567890 # Your network key**
================================
````

Configure DHCP so that the device asks the DHCP server for an IP address:  
````
$ vi /etc/config/network
````

Change the lan interface (eth0.1) like this:  
````
================================

config 'interface' 'lan'
option 'ifname'  'eth0.1'
option 'proto'  'dhcp'
================================
````

Now you can start WiFi like this:  
````
$ wifi up
````

After that you can unplug the LAN cable. The router should be connected to your WLAN and it should acquire an IP address from your local network DHCP server. Make sure that you do not gave a MAC Filter enabled in case it does not work ;)  

For an intial test you can updload a MP3 file to the device:  
````
$ scp USER <user>@HOST<host>:HOME<home>/test.mp3 /tmp</home></host></user>
````

With madplay the MP3 can be played like this:  
````
$ madplay /tmp/test.mp3
````

With the following script running on your desktop machine you can make the Music Play Daemon play a Shoutcast audio stream:  
````
$ export MPD_HOST=192.168.0.111
$ mpc add http://89.179.179.5:8040
$ mpc play
````
