---
layout: single
title: Compile a development version of GUPnP from HEAD
date: '2011-06-13T17:53:00.001+02:00'
author: Michael Rumpf
tags:
- audio
- upnp
- openwrt
modified_time: '2011-07-17T18:33:48.876+02:00'
categories:
---

Follow the commands below in order to setup a local environment for working with the latest development version of the GUPNP framework.  

````
mkdir $(pwd)/base  
git clone http://git.gnome.org/browse/gssdp  
cd gssdp  
./autogen.sh --prefix=$(pwd)/../base --exec-prefix=$(pwd)/../base  
make && make install  
cd ..  
git clone http://git.gnome.org/browse/gupnp  
cd gupnp
PKG_CONFIG_PATH=$(pwd)/../base/lib/pkgconfig ./autogen.sh --prefix=$(pwd)/../base --exec-prefix=$(pwd)/../base
cd ..  
git clone http://git.gnome.org/browse/gupnp-tools  
cd gupnp-tools  
PKG_CONFIG_PATH=$(pwd)/../base/lib/pkgconfig ./autogen.sh --prefix=$(pwd)/../base --exec-prefix=$(pwd)/../base
cd ..
````

Run the following commands for setting the environment to use the fresh compiled libraries instead of the OS provided ones (Empathy under Ubuntu 11.04 uses an older version of the GUPNP libraries. I ran into segmentation faults because the libraries did not match):

````
export PATH=$(pwd)/base/bin:$(pwd)/gupnp/examples:$PATH
export LD_LIBRARY_PATH=$(pwd)/base/lib:$LD_LIBRARY_PATH
export PKG_CONFIG_PATH=$(pwd)/base/lib/pkgconfig:$PKG_CONFIG_PATH
````

Now you can execute several examples or tests to check whether everything works as expected:

````
light-server &

gupnp-universal-cp &
````

The GUI application GUPnP Universal Control Point shows an entry "Kitchen Lights", which is provided by the light-server application started above.