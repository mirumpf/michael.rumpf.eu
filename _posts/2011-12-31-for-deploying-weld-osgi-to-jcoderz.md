---
layout: single
title: Deploying weld-osgi to jcoderz.org repository
date: '2011-12-31T16:36:00.000+01:00'
author: Michael Rumpf
tags:
- osgi
- maven
- java
modified_time: '2012-01-02T09:56:40.954+01:00'
categories:
---

For deploying weld-osgi to the jcoderz.org repository the following modifications to the `pom.xml` 
files are necessary (after applying the wagon-ssh extensions to the your Maven installation, see previous post):  

1. Deploy the WELD API packages  

````
git clone -b WELD-963 http://github.com/mathieuancelin/api.git WELD-963
cd WELD-963
vi pom.xml
````

Add the following XMl fragment to the maven-deploy-plugin

````
   <build>  
      <plugins>  
         <plugin>  
            <groupId>org.apache.maven.plugins</groupId>  
            <artifactId>maven-deploy-plugin</artifactId>  
            <configuration>  
               <skip>true</skip>  
            </configuration>  
            <dependencies>  
              <dependency>  
                <groupId>org.apache.maven.wagon</groupId>  
                <artifactId>wagon-ssh</artifactId>  
                <version>2.2</version>  
              </dependency>  
            </dependencies>
         </plugin>  
      </plugins>  
   </build>
````
  
````
mvn -e clean install -Dmaven.test.skip=true
mvn -e deploy -DaltDeploymentRepository=jcoderz-snapshots::default::scp://jcoderz.org/var/www/repos/maven2-snapshot/ -Dmaven.test.skip=true
cd ..  
````
   
2. Deploy the WELD OSGi packages  

````
git clone -b weld-osgi http://github.com/mathieuancelin/core.git weld-osgi
cd weld-osgi
vi pom.xml
````

Add the following XML into the <build> tag section:

````
         <plugin>  
           <groupId>org.apache.maven.plugins</groupId>  
           <artifactId>maven-deploy-plugin</artifactId>  
           <dependencies>  
             <dependency>  
               <groupId>org.apache.maven.wagon</groupId>  
               <artifactId>wagon-ssh</artifactId>  
               <version>2.2</version>  
             </dependency>  
           </dependencies>  
         </plugin>
`````

````
mvn -e clean install -Dmaven.test.skip=true  
mvn -e deploy -DaltDeploymentRepository=jcoderz-snapshots::default::scp://jcoderz.org/var/www/repos/maven2-snapshot/ -Dmaven.test.skip=true  
cd environments/osgi  
````

Deploy weld-osgi-parent pom.xml to the repository (Is there a way to include the parent pom.xml in the previous run?)  
````
mvn -e -N deploy -DaltDeploymentRepository=jcoderz-snapshots::default::scp://jcoderz.org/var/www/repos/maven2-snapshot/ -Dmaven.test.skip=true
````
With those deployments the repository is prepared to compile the m3server project...