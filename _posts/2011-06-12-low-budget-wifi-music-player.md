---
layout: single
title: Low-budget WiFi Music Player
date: '2011-06-12T13:17:00.002+02:00'
author: Michael Rumpf
tags:
- audio
- linux
- openwrt
modified_time: '2011-07-17T18:34:00.682+02:00'
categories:
---

After searching the net for quite a while I could not find a pure wireless music player for small money. In case 
you want to equip multiple rooms in your house with a music player it will cost you a lot of money. All products
on the market come with a lot of features, of which most are not necessary. The following links show interesting 
hacks on how to build your own WiFi music player based on OpenWRT:
        
*   [http://www.instructables.com/id/Build-your-own-Wifi-radio/](http://www.instructables.com/id/Build-your-own-Wifi-radio/)
*   [http://robhardwick.wordpress.com/2008/07/21/3/](http://robhardwick.wordpress.com/2008/07/21/3/) 
*   [http://devices.natetrue.com/musicap/](http://devices.natetrue.com/musicap/)
*   [http://neophob.com/2008/02/openwrt-mp3-player-screenshots/](http://neophob.com/2008/02/openwrt-mp3-player-screenshots/)
*   [http://www.thilloy.com/integrations/musicplayer.html](http://www.thilloy.com/integrations/musicplayer.html)
*   [http://mightyohm.com/blog/2008/10/building-a-wifi-radio-part-1-introduction/](http://mightyohm.com/blog/2008/10/building-a-wifi-radio-part-1-introduction/)
*   [http://blog.josefsson.org/2007/09/25/home-audio-server/](http://blog.josefsson.org/2007/09/25/home-audio-server/)

Using routers as a platform is a very good choice as they produced in high volume and thus the price for the  
hardware can be very low. Every platform evaluation board is much more expensive, especially when you  
want to have WiFI connectivity (> EURO 150). Those evaluation boards or development kits are produced  
in very small volume and thus the price is not competitive.  

I decided to use a router as the base for my player. My current setup is this:

*   [TP-Link TL-WR1043ND](http://www.tp-link.com/de/products/productDetails.asp?pmodel=TL-WR1043ND): EUR 43,59
*   [3D-Sound USB-Audio](http://www.amazon.de/gp/product/B003IVHBDW/ref=oss_product): EUR 7,48
*   [Delock HUB USB 2.0 extern 4 Port](http://www.amazon.de/gp/product/B000P1RQSI/): EUR 12,49
*   32MB USB Stick: EUR 3,00 (for a 1GB Stick) 

This makes EUR 66,56 for the music player without loud-speakers. I guess the price can be further reduced  
because the sound-card was more expensive than expected (additional shipping costs), and there are much  
cheaper USB Hubs available, but this one I already had around.  
The router TP-Link TL-WR1043ND is the TP-Link flagship consumer product, choosing another (maybe a  
used one) one where you can easily solder a USB port, would also do.  

For the second version of the music player I expect the price to drop around EUR 30.
