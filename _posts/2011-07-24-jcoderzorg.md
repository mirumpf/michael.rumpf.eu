---
layout: single
title: jCoderZ.org
date: '2011-07-24T13:31:00.001+02:00'
author: Michael Rumpf
tags:
- audio
- openwrt
modified_time: '2012-06-25T23:40:33.555+02:00'
categories:
---

The OpenWRT music player, which I have blogged about recently, has found a  
home at [https://github.com/jCoderZ/m3player](https://github.com/jCoderZ/m3player)