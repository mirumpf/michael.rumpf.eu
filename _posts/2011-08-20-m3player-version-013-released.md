---
layout: single
title: m3player version 0.1.3 released
date: '2011-08-20T01:45:00.002+02:00'
author: Michael Rumpf
tags:
- audio
- openwrt
modified_time: '2011-08-20T01:56:41.357+02:00'
categories:
---

[m3player version 0.1.3](http://www.jcoderz.org/m3dditiez/wiki/m3ddity-player) has the following new features:  

*   m3player can be run as a daemon with `-d` or `--daemon`
*   The name of the UPnP Media Renderer can be configured by `-n` or `--name`. This is done by copying 
the XML files from `/usr/share/m3player` to `/var/lib/m3player` and filtering place-holders in some of the files.
*   The version of the MediaRenderer device can be configured by `-r` or `--root`. The default is `MediaRendererV2.xml`
*   The UPnP server can be bound to a network interface, specified by `-i` or `--interface`. Example: eth0

The help output shows an overview of all the parameters available:  

````
$ ./m3player -h  
Usage:  
  m3player [OPTION...]

Help Options:  
  -h, --help          Show help options  

Application Options:  
  -c, --config        Path to the config file   
                      (Default: /etc/m3player/m3player.ini)  
  -p, --pid           Path to the pid file   
                      (Default: /var/run/m3player.pid)  
  -s, --xmlsource     Path to the XML source folder   
                      (Default: /usr/share/m3player)  
  -t, --xmltarget     Path to the XML target folder   
                      (Default: /var/lib/m3player)  
  -n, --name          The name of the player instance   
                      (Default: HOSTNAME)  
  -r, --root          The name of the root device file   
                      (Default: MediaRendererV2.xml)  
  -l, --log           The name of the log file   
                      (Default: /var/log/m3player.log)  
  -i, --interface     The name of the interface the process  
                      is bound to (e.g. eth0)  
  -d, --daemonise     Fork the player as daemon
````