---
title:  "MR Computer"
layout: single
permalink: /projects/mrcomputer
author_profile: true
comments: false
---

This application has been developed for supporting a small business in selling hardware.

A customer orders one or more hardware items. This order has to be entered by the user of the application.

The user can choose hardware items from different dealers. The software splits the orders automatically into orders to the different dealers, selected implicitly by the user.

When the items from the dealers arrive they have to be marked as delivered and thus show up in the invoices, created for each of the customers.

[![Main Screen]({{"/assets/images/mrcmainsmall.png" | absolute_url}})]({{"/assets/images/mrcmain.png" | absolute_url}})  
*Main Screen*

[![Enter Orders]({{"/assets/images/mrcbestellsmall.png" | absolute_url}})]({{"/assets/images/mrcbestell.png" | absolute_url}})  
*Enter Orders*

[![Customer Administration]({{"/assets/images/mrckundensmall.png" | absolute_url}})]({{"/assets/images/mrckunden.png" | absolute_url}})  
*Customer Administration*

[![Dealer Administration]({{"/assets/images/mrchaendlersmall.png" | absolute_url}})]({{"/assets/images/mrchaendler.png" | absolute_url}})  
*Dealer Administration*

[![Fax Orders]({{"/assets/images/mrcbestellfaxsmall.png" | absolute_url}})]({{"/assets/images/mrcbestellfax.png" | absolute_url}})  
*Fax Orders*

[![Print Invoices]({{"/assets/images/mrcrechnungsmall.png" | absolute_url}})]({{"/assets/images/mrcrechnung.png" | absolute_url}})  
*Print Invoices*

|||
| --- | --- | 
| Type | Freelancer Project | 
| Environment | Windows 9x/NT, Microsoft Visual Basic 5, Microsoft Access 6.0, ODBC | 
| Link | [mrcomputer.tar.bz2]({{"/assets/files/mrcomputer.tar.bz2" | absolute_url}}) (85kB) | 