---
title:  "T-Systems"
layout: single
permalink: /projects/tsystems
author_profile: true
comments: false
---

During my time at T-Systems I worked on the following projects:

# Daimler Vehicle Backend

Beginning of 2013 I started as the architect of the Vehicle Homepage project, which is a part of the Daimler Vehicle Backend. The whole solution was originally designed on top of an enterprise service bus and application server eco system. In 2015 we started a major refactoring of the platform and split up the monolith services into smaller microservices, using the Spring Boot, Spring Cloud, and Netflix OSS stack. In 2018 this refactoring will be completed as a pre-requisite of moving the whole system from a traditionally operated data center to a hybrid cloud setup.  

# Software Build Service

The "Software Build Service" (SBS) originated in the AMS@ITP project (see below). Since then it has evolved to the standard CI/CD platform within T-Systems.  
Currently more than 50 projects and over 500 users are using the service.  
The hosted platform gets updated on a regular basis, making sure that the latest features of the different tools (Jenkins, Nexus, SonarQube) are available.

# Prototype: Teilebedarfsermittlung*

T-Systems has developed and maintains the software to calculate the parts that are necessary for the Daimler vehicle production. This software is written in COBOL and runs on an IBM mainframe system. The idea of the prototype was to re-implement the core calculation logic in Java and to demonstrate how commodity hardware could be used to scale the system.  
The focus was to keep the calculation logic separate from any infrastructure code (JEE) so that it could easily be tested and to demonstrate the flexibility, we migrated the algorithm to an Android tablet, with a very simple user-interface.  
The prototype was a big success in that we could demonstrate that the full resolution could easily be divided into smaller parts and by horizontally scaling the time could be reduced almost arbitrarily.

# Dynamic Services for Developers

The Dynamic Services 4 Developers (DS4D) project was an attempt to create a self-provisioning system for external customers, where they could setup development environments for their projects in a public cloud offered by T-Systems.  
Me and colleagues got involved because of the experiences with the "Software Build Service", which was planned to be an integral part of the cloud offering.

# Application Management Service for the Daimler IT management organization

The challenge in this project was that around 50 Java and a few .NET applications were to be shifted from small suppliers to T-Systems. My job was to design a system which standardized the way the software was built and delivered to the customer.  
The solution was built on top of Jenkins, Artifactory, and Sonar. The biggest effort was to migrate the extremely variety of build scripts to a standardized approach.  
The experiences from this project led to the creation of the "Software Build Service".

|||
| --- | --- | 
| Type | Senior Software Architect | 
| Environments | Windows, Linux, Eclipse/STS, Tomcat, IBM WebSphere 6.1/7.0, Maven, Ant, Java 8, JEE 6/7, Jenkins, Nexus, SonarQube, Android, Spring, Spring Boot, Spring Cloud Findbugs, Checkstyle, PMD, Daimler Pro Active Infrastructure (PAI) 4.0 - 5.0 | 