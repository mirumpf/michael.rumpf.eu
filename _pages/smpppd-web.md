---
title:  "SMPPPD Web"
layout: single
permalink: /projects/smpppd-web
author_profile: true
comments: false
---

An extension of the web interface to the SuSE Meta PPP Daemon.

Christopher Hofmann has created a nice web-based front-end to the SuSE Meta PPP Daemon (SMPPPD).[smpppd-web](http://www.suse.de/~cwh/)

![]({{"/assets/images/smpppd-web1small.png" | absolute_url}})

![]({{"/assets/images/smpppd-web2small.png" | absolute_url}})

![]({{"/assets/images/smpppd-web3small.png" | absolute_url}})

![]({{"/assets/images/smpppd-web4small.png" | absolute_url}})

My extension to the original codebase was to support adding and removing ISDN links on the fly and allowing dial-on-demand of ISDN interfaces.

My main user could speak german only so that I added gettext support to the application and translated all texts into german as well.


|||
| Type | Personal Project | 
| --- | --- | 
| Environment | Linux, vi, XSL, HTML Tidy, expect | 
| Link | [smpppd-web.tar.bz2]({{"/assets/files/smpppd-web.tar.bz2" | absolute_url}}) (17kB), [smpppd-web-3.00-0.1.i586.rpm](http://www.suse.de/~cwh/rpms/10.0/) (packaged by Christopher Hofmann) | 