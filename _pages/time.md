---
title:  "ORBit Time Service"
layout: single
permalink: /projects/time
author_profile: true
comments: false
---

A CORBA TimeService implementation developed for a CORBA ORB with C language mapping.

When I started using Linux on the desktop, I got interested in the small and efficient CORBA ORB, called ORBit. The distributed component architecture [CORBA](http://www.omg.org/ "Common Object Request Broker Architecture") is the default inter-process communication protocol used in the [GNOME](http://www.gnome.org/ "GNU Network Object Model Environment") desktop project.

Teaching myself the [CORBA C Language Mapping](http://www.omg.org/technology/documents/formal/c_language_mapping.htm), which ORBit provides, I wrote the CORBA 'TimeService' on top of the ORBit ORB.

|||
| --- | --- | 
| Type | Personal Project | 
| Environment | Linux, ORBit 0.5.x, C, GCC, vi, GNU make | 
| Link | [orbit-time.tar.bz2]({{"/assets/files/orbit-time.tar.bz2" | absolute_url}}) (30kB) | 