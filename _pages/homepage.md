---
title:  "Homepage"
layout: single
permalink: /projects/homepage
author_profile: true
comments: false
---

The Homepage project shows the evolution of this website.

The first version of the homepage was simply a project to teach myself HTML. Since version 2.0 I started using it as an online reference page when applying for a new job.

# Version 1.0 (199?/?? - 1997/12)
[![Homepage 1.0]({{"/assets/images/homepage1small.png" | absolute_url}})]({{"/assets/images/homepage1.png" | absolute_url}})
*Homepage 1.0*

The menu in this version was rendered with the 3D ray-tracing application Lightwave 3D on the Amiga. The big disadvantage of using a full blown image as a menu, is that the menu took a long time to load at the time when 28.8 Modems were state of the art.

# Version 2.0 (1997/12 - 2001/11)
[![Homepage 2.0]({{"/assets/images/homepage2small.png" | absolute_url}})]({{"/assets/images/homepage2.png" | absolute_url}})
*Homepage 2.0*

In this version HTML 3.2, CGI, Perl, a Java Applet, and JavaScript was used.

Special feature modules:

*   Abi91-Access Control (htpasswd, htaccess)
*   Abi91-Database (Perl, CSV File)
*   Abi91-Guestbook (Perl)
*   Chatrooms (Abi91/TSC) (Java-Applet)

# Version 3.0 (2001/11 - 2004/06)
[![Homepage 3.0]({{"/assets/images/homepage3small.png" | absolute_url}})]({{"/assets/images/homepage3.png" | absolute_url}})
*Homepage 3.0*

For the third version of the homepage I dropped all the fancy features that led to incompatibilities with different web browsers. The only technologies used were XHTML 1.0 and CSS. Unfortunately at that time the current browsers were not able to render the page as the standard expected them to. Therefore I added a picture how the page looks like when using a compliant browser. The reason for doing this was a general refusal to waste my time by testing the page with many different browser versions just to develop workarounds for bugs that the browser vendor were expected to fix.

# Version 4.0 (2004/06 - 2006/02)
[![Homepage 4.0]({{"/assets/images/homepage4small.png" | absolute_url}})]({{"/assets/images/homepage4.png" | absolute_url}})
*Homepage 4.0*

This version uses 'XHTML 1.0 Transitional' and CSS. All the current browsers can render the pages without known problems.

For this version I have written a bunch of scripts that automate many tasks building the homepage:

*   Conversion of TIFF files into password protected PDFs
*   Link checking of all pages (HTML + LaTex) with linkchecker
*   Spell checking of all pages (HTML + LaTex) with ispell
*   Automatic upload to the ISP storage facility

# Version 5.0 (2006/03 - 2019/01)
[![Homepage 5.0]({{"/assets/images/homepage5small.png" | absolute_url}})]({{"/assets/images/homepage5.png" | absolute_url}})
*Homepage 5.0*

This version of my homepage is similar to version 4.0 with an updated layout.

# Version 6.0 (2019/02 - present)
[![Homepage 6.0]({{"/assets/images/homepage6small.png" | absolute_url}})]({{"/assets/images/homepage6.png" | absolute_url}})
*Homepage 6.0*

This version has been transformed from the previous version to a [Jekyll](https://jekyllrb.com/) based static page, using the [Minimal Mistakes](https://github.com/mmistakes/minimal-mistakes) theme.

|||
| --- | --- | 
| Type | Personal Project | 
| Environment | Jekyll Static Page Generator, Lightwave 3D, Visual Studio 97, vi, Ultraedit, Bluefish, Perl, JavaScript, HTML 3.2, XHTML1.0/CSS1, CSV Files, Apache, bash, pdftk, awk | 