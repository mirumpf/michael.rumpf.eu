---
title:  "Clearing and Settlement System"
layout: single
permalink: /projects/css
author_profile: true
comments: false
---

The clearing and settlement solution was developed between September 2004 and June 2005 to process huge amounts of chargeabe events by creating files that could be used to do the actual settlements with a bank.

[![Architecture]({{"/assets/images/cssng_small.png" | absolute_url}})]({{"/assets/images/cssng.png" | absolute_url}})  
*Architecture*

My task in this project was to develop the Finance Calculation Service (FCS). The major design goal of the FCS was to keep the interface as simple as possible and to avoid any dependencies to other components of the application. The workflow engine component was responsible for the orchestration of all the different services. It reads the financial base data via the Finance Data Repository (FDR) out of the database and feeds these entities into the API of the FCS. The calculated results are stored via the FDR into the database again. The design follows the dependency injection principle which feeds all data into the methods of the FCS component. It allows the service to be used locally without the need for an EJB container or a database.

I also developed the graphical user-interface, an online reporting view on the settled payments, by following the Model View Controller (MVC) pattern.

| | |
| --- | --- | 
| Type | Senior Software Architect at First Data Mobile Solutions | 
| Environment | Linux, AIX, Eclipse, BEA Weblogic 8.1, EJB, JUnit, JSP, Ant, JSTL | 