---
title:  "PCon 3.0"
layout: single
permalink: /projects/pcon30
author_profile: true
comments: false
---

This was a rewrite of the productivity controlling application 1.2 using the Microsoft Foundation Classes (MFC) to achieve a more appealing look and feel and to simplify the user-interface.

In version 3.0 the following features have been implemented or removed:

*   Added a flexible employee model (employees, groups of employees, and machines).
*   Added support for data terminals.
*   Implemented a traceability system for base-data.
*   Added flexible task types (single, combined, group, and long-running).
*   Used DAO (Data Access Objects) for database access.
*   Implemented a comfortable installation procedure.
*   Added data import from ERP systems for employee data.
*   Used Crystal Reports V5.0 for reporting.
*   Added flexible customer management.
*   Supported more Win32 APIs.
*   Removed the hard to understand number system.
*   Dropped the hour type concept as it was not in line with the productivity calculations.
*   Dropped the macro system.

[![Main Window]({{"/assets/images/pcon30main_small.png" | absolute_url}})]({{"/assets/images/pcon30main.png" | absolute_url}})
*Main Window*

The main window above shows all the available menus and the two button bars. The top bar provides quick access to the application's main functions. The right bar offers access to the reporting system and the status bar is showing the currently active company.

[![Base Data]({{"/assets/images/pcon30stammdaten_small.png" | absolute_url}})]({{"/assets/images/pcon30stammdaten.png" | absolute_url}})
*Base Data*

The base data dialog leads to specifying the company's hierarchical structure, i.e. departments, tasks, capacities, terminals, pauses, and employees. Employees need to be imported from one of two supported human resource management systems. The number of supported systems could easily be extended as a plugin architecture was chosen to access employee data from these external applications.

# Traceability System (Author: Stefan Bayer)

[![Task Changes]({{"/assets/images/dokuarbeitensmall.png" | absolute_url}})]({{"/assets/images/dokuarbeiten.png" | absolute_url}})
*Task Changes*

[![Capacity Changes]({{"/assets/images/dokukapazitaetensmall.png" | absolute_url}})]({{"/assets/images/dokukapazitaeten.png" | absolute_url}})
*Capacity Changes*

[![Department Changes]({{"/assets/images/dokuorganesmall.png" | absolute_url}})]({{"/assets/images/dokuorgane.png" | absolute_url}})
*Department Changes*

The traceability system is used to track changes to all the base data. The dialogs above show modifications to any of the data types.

# Task Data
[![Task Data]({{"/assets/images/pcon30arbeitsdaten_small.png" | absolute_url}})]({{"/assets/images/pcon30arbeitsdaten.png" | absolute_url}})
*Task Data*

The task data dialog allows input of new data and changes to existing data:

*   Single Task  
    For a single task the amount and the time used are known at the end of the task. That means calculation of a productivity is immediately possible.
*   Combined Task  
    Combined tasks are tasks that can't be separated easily. At the end, the amount and the time to completion is known. The time will be distributed to each of the single tasks and from that some kind of productivity is calculated.
*   Group Task  
    A group task is just the opposite of a combined task. In a group task multiple employees are involved. At the end of the day one employee from the group enters the amount that has been achieved by the group. The amount will then be distributed to the employees of the group and again some kind of productivity can be calculated.
*   Long-Running Task  
    A long-running task lasts several days. At the end of each day the time is entered into the terminal but no amount. At the end of the task the amount is split over the time that has been aggregated in the meantime. In this case, as in all the previous cases, some kind of productivity is calculated from the values.

When entered, each task can be classified as productive or deficient. A deficient task must be further qualified whether the produced good was reworked or whether the task had to be completely rolled back and restarted.

# Statistical Reports (Design: Frank Blinn)

[![Daily Productivity]({{"/assets/images/ausw_tagprodsmall.png" | absolute_url}})]({{"/assets/images/ausw_tagprod.png" | absolute_url}})

[![Productivity-Time Balance Distribution (Departments)]({{"/assets/images/ausw_verlprodzeitsaldorgansmall.png" | absolute_url}})]({{"/assets/images/ausw_verlprodzeitsaldorgan.png" | absolute_url}})

[![Task Type Distribution]({{"/assets/images/ausw_vertarbartsmall.png" | absolute_url}})]({{"/assets/images/ausw_vertarbart.png" | absolute_url}})

[![Productivity Distribution]({{"/assets/images/ausw_verlprodsmall.png" | absolute_url}})]({{"/assets/images/ausw_verlprod.png" | absolute_url}})

[![Amount-Time Distribution]({{"/assets/images/ausw_verlmengzeitsmall.png" | absolute_url}})]({{"/assets/images/ausw_verlmengzeit.png" | absolute_url}})

[![Amount Distribution]({{"/assets/images/ausw_verlmengergebnissmall.png" | absolute_url}})]({{"/assets/images/ausw_verlmengergebnis.png" | absolute_url}})

[![Distribution Task Times]({{"/assets/images/ausw_verlverbrzeitensmall.png" | absolute_url}})]({{"/assets/images/ausw_verlverbrzeiten.png" | absolute_url}})

[![Distribution Deficiency Tasks]({{"/assets/images/ausw_verlmaengelanteilesmall.png" | absolute_url}})]({{"/assets/images/ausw_verlmaengelanteile.png" | absolute_url}})

[![Productivity-Time Balance Distribution (Tasks)]({{"/assets/images/ausw_verlprodzeitsaldarbeitsmall.png" | absolute_url}})]({{"/assets/images/ausw_verlprodzeitsaldarbeit.png" | absolute_url}})


# Employee Management

[![Main Dialog]({{"/assets/images/pcon30mitarbeiterverwaltung_small.png" | absolute_url}})]({{"/assets/images/pcon30mitarbeiterverwaltung.png" | absolute_url}})
*Main Dialog*

For PCon 3.0 two import modules have been developed, one for the own employee management software and the other one for 'Zesy 2.0' from _Gisbo Softwareentwicklung und EDV-Beratung GmbH_.

# Customer Management (Author: Stefan Bayer)

The customer management system can be used to create, modify, and delete customers.

[![Main Dialog]({{"/assets/images/pcon30mandantenmanager_small.png" | absolute_url}})]({{"/assets/images/pcon30mandantenmanager.png" | absolute_url}})
*Main Dialog*

[![New-/Modify-Dialog]({{"/assets/images/pcon30mandantbearbeiten_small.png" | absolute_url}})]({{"/assets/images/pcon30mandantbearbeiten.png" | absolute_url}})
*New-/Modify-Dialog*

|||
| --- | --- | 
| Type | Software Developer at Dr. Tackenberg GmbH | 
| Environment | Windows 9x/NT, Visual C++ 4.0, MFC, Crystal Reports | 