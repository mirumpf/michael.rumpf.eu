---
title:  "Person to Person Payment"
layout: single
permalink: /projects/ppy
author_profile: true
comments: false
---

An application for mobile phone based money transfers.

The **P**erson to **P**erson pa**Y**ment (PPY) prototype has been developed in just a week as a show-case for how a traditional money transfer system could be extended by a mobile phone channel, utilizing SMS for sending money orders.

[![PPY Transaction List]({{"/assets/images/ppy_small.png" | absolute_url}})]({{"/assets/images/ppy.png" | absolute_url}})
*PPY Transaction List*

My task was to create the web channel for the PPY prototype. XHTML and CSS, together with Java Server Pages (JSP) and the JSP Standard Tag Library (JSTL) was chosen to develop the page templates. The page flow of the prototype has been kept as simple as possible. The prototype's intention, besides showing the functionality to an end-user, was to gather experience in the area of graphical user-interfaces, as our focus in the past has been server side software only with no or just a little need for graphical user-interfaces.

|||
| --- | --- | 
| Type | Senior Software Architect at First Data Mobile Solutions | 
| Environment | Linux, AIX, Eclipse, BEA Weblogic 8.1, XHTML, CSS, JSP, JSTL | 