---
title:  "CORBA/CGI Bridge"
layout: single
permalink: /projects/corbacgi
author_profile: true
comments: false
---

The CORBA/CGI bridge is an evaluation prototype for testing how a CGI application could be substituted by CORBA components.

The idea behind this prototype was to substitute a CGI application, which was accessing an application on the server, by a CORBA layer. The client application had to be left unchanged, as the source code was not available.

In a first step the CGI application had been substituted by a Java servlet deployed on Sun's Java Web Server. It was still accessing the server application by executing a command line script.

[![CORBA-CGI Architecture]({{"/assets/images/corbacgi_small.png" | absolute_url}})]({{"/assets/images/corbacgi.png" | absolute_url}})  
*CORBA-CGI Architecture*

In the second step the server application was wrapped by a CORBA IDL interface and accessed by a Java CORBA client from out of the servlet. The CORBA server was written in C++, using the VisiBroker CORBA ORB. The servlet was using Sun's JavaIDL CORBA ORB as a client.

The servlet provides the same interface as the original CGI program. It is just parsing the parameters of the HTTP request and passing them to the interface of the CORBA server application. The benefit of using CORBA is that the server side application could be moved to another machine with the network communication handled transparently by the CORBA IIOP protocol.

| | | 
| --- | --- | 
| Type | Internship, Evaluation Project | 
| Environment | Windows 9x/NT4, C++, Java, JDK 1.2 Beta, JavaIDL 1.0, VisiBroker C++ 3.2, JavaWebServer 2.0 Beta | 