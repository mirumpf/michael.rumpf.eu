---
title:  "PCon 3.5"
layout: single
permalink: /projects/pcon35
author_profile: true
comments: false
---

This extension of productivity controlling 3.0 added concurrent user access functionality to the application.

The most important feature in version 3.5 of the [Productivity-Controlling (PCon)]({{"/projects/pcon30" | absolute_url}}) application is the concurrent user capability. The original design did not allow multiple users to access the Microsoft Access database concurrently. Unfortunately the Access database does not provide a concurrent access feature and thus the only way to fix this design flaw easily was to introduce a DCOM server component, monitoring access to the database and keeping track of the connections.

As soon as one user is accessing a record the record is marked as locked at the DCOM Locking server. When another user tries to access the same record he gets a message that the record is currently in use and can not be accessed. In the list view a lock is shown by a red prohibition sign.

Another feature is the full implementation of the application's help system with Doc2Help 4.0. In each screen the question mark button and the F1 key for accessing this help system is working.

|||
| --- | --- | 
| Type | Software Developer at Dr. Tackenberg GmbH | 
| Environment | Windows 9x/NT, Microsoft VisualStudio 6 (Enterprise Edition), C++, MFC | 