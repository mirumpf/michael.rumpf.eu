---
title:  "Inventory Management"
layout: single
permalink: /projects/inventory
author_profile: true
comments: false
---

A solution to create offers from product description fragments, stored in an MS Access database.

This prototype emerged from the VBA project [Autotexts]({{"/projects/autotexts" | absolute_url}}). The problem with the Microsoft Word Autotext solution was that it did not scale, it was not suitable for concurrent access, and it was hard to maintain the set of Autotext fragments in a simple Word document template. The company indicated that they were interested in a more professional solution but later decided to go with an already existing and established application.

[![Main Window]({{"/assets/images/avwsmall.png" | absolute_url}})]({{"/assets/images/avw.png" | absolute_url}})  
*Main Window*

In this prototype products are assigned to categories. The list of categories can be extended arbitrarily.

![New Product]({{"/assets/images/avwnew.png" | absolute_url}})  
*New Product*

The button "Edit" can be used to edit the product description.

With "Settings" the template can be specified on which a final offer document will be based. Also the address database with the customer addresses can be specified here.

![Settings]({{"/assets/images/avwsettings.png" | absolute_url}})  
*Settings*

The button "New Offer" creates a new document based on the specified template. The address comes from the customer address database and the chosen product is attached to the document.

[![Offer-Template]({{"/assets/images/avwwordsmall.png" | absolute_url}})]({{"/assets/images/avwword.png" | absolute_url}})  
*Offer-Template*

![MS-Access Address Database]({{"/assets/images/avwaccess.png" | absolute_url}})  
*MS-Access Address Database*

|||
| Type | Freelancer Project Prototype | 
| --- | --- | 
| Environment | VisualBasic 6, Office 97 Professional (Word, Access), ODBC, OLE/COM | 
| Link | [inventory.tar.bz2]({{"/assets/files/inventory.tar.bz2" | absolute_url}}) (8MB) | 