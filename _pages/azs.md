---
title:  "Automatic Zapit Satellite"
layout: single
permalink: /projects/azs
author_profile: true
comments: false
---

This software provides a set of scripts for the automatic generation of DBox2 Neutrino channel lists as a work-around for an automatic transponder scan which does not work on all DBox2 boxes.

**A**utomatic **Z**apit **S**attellite (AZS) is a small collection of scripts to extract the [Astra](http://www.ses-astra.com) digital TV and digital radio channel lists from internet sources and to create the files `services.xml` and `bouquets.xml` that are used by the Linux operating system on the digital satellite receiver DBox2.

Current [yadi](http://www.yadi.org) Neutrino versions come with rather sophisticated channel scanning software. Unfortunately this is not working with my DBox2\. Most probably the signal quality of my DBox2 is not good enough because trees might be blocking the view. This problem manifests in the fact that an automatic channel scan detects only a small amount of channels, but when creating a channel list from Internet sources all channels are displayed without problems.

The current AZS version gets the channel list from [Lyngsat](http://www.lyngsat.com), which provides a really broken HTML page with all the digital TV and radio channels. The script cleans up the page using HTML tidy and strips it down by using a XSL stylesheet. The result is a relatively clean and simple HTML page and is used to create the `services.xml` file, which contains the channel to Satellite transponder assignment. In the next stage this file serves as the source for the `bouquet.xml` file, a channel to bouquet assignment. This AZS version uses a file called `mybouquets.xml` which contains a hint for the scripts which channels go into which bouquets. The final step is the automatic upload of the files to the DBox2 via FTP by using the `except` tool.

In the current version it is necessary to manually create the `mybouquets.xml`. This file is actually a `services.xml` file with a different bouquet assignment. When running AZS for the first time the `services.sh` script must be executed and the generated `services.xml` file must then copied to the file `mybouquets.xml`. The user needs to adapt his bouquet assignments so that the final `services.xml` can be generated on the fly when running the scripts for the second time.

----
./services.sh
cp services.xml mybouquets.xml
# Edit the mybouquets.xml and adapt the channel to bouqets assignment
----

When executing the `azs.sh` script subsequently, the adapted `mybouquets.xml` is used to rearrange the `services.xml` and finally upload the generated files on to the DBox2.

----
./azs.sh <HOST>
----

| | | 
| --- | --- | 
| Type | Personal Project | 
| Environment | Linux, XSL, Bash, HTML Tidy, expect | 
| Link | [azs-0.2.tar.bz2]({{"/assets/files/azs-0.2.tar.bz2" | absolute_url}}) (21kB) | 
