---
title:  "Projects"
layout: single
permalink: projects/
author_profile: true
comments: false
---
Since 1985 I have been working on the following software development projects:

| Project | Year | Description |
| --- | --- | --- |
| [Daimler]({{"projects/daimler" | absolute_url}}.html) | 2019 | Working as a cloud software architect for the Daimler Vehicle Backend (DaiVB) |
| [IBM]({{"projects/ibm" | absolute_url}}.html) | 2018-2019 | Working as a cloud application innovation architect for the Daimler Vehicle Backend (DaiVB) |
| [T-Systems]({{"projects/tsystems" | absolute_url}}.html) | 2010-2018 | Working as the technical lead architect for the Daimler Vehicle Backend (DaiVB) and establishing a shared service, called the Software Build Service (SBS), a CI/CD platform for internal projects. |
| [Achievo]({{"projects/achievo" | absolute_url}}.html) | 2006-2010 | The Achievo Development Framework (ADF) and two large J2EE 1.4 projects TACO and DFM |
| [Automatic Zapit Satellite]({{"projects/azs" | absolute_url}}.html) | 2005-2006 | Automatic generation of TV channel lists for Linux on the DBox2 |
| [smpppd-web]({{"projects/smpppd-web" | absolute_url}}) | 2005 | Extension of a web interface for the SuSE Meta PPP Daemon |
| [Micro AGGregation Engine]({{"projects/maggie" | absolute_url}}.html) | 2005 | J2EE prototype application for micro payment aggregations |
| [Person 2 Person Payment]({{"projects/ppy" | absolute_url}}.html) | 2005 | J2EE prototype application for SMS person-to-person payments |
| [Clearing and Settlement System]({{"projects/css" | absolute_url}}.html) | 2004-2005 | J2EE based system for clearing and settlement of financial transactions |
| [Twister]({{"projects/twister" | absolute_url}}.html) | 2000-2004 | The Brokat/Encorus J2EE application server |
| [CORBA TimeService]({{"projects/time" | absolute_url}}.html) | 2000 | A CORBA TimeService implementation in C |
| [ORBit-Resource]({{"projects/orbit" | absolute_url}}.html) | 2000 | A central repository of ORBit project resources |
| [CORBA 2.2 Validator]({{"projects/validator" | absolute_url}}.html) | 2000 | An IDL compiler compliance test suite |
| [Inventory Management]({{"projects/inventory" | absolute_url}}.html) | 1999 | Inventory management prototype as successor of the Autotexts project |
| [Autotexts]({{"projects/autotexts" | absolute_url}}.html) | 1999 | Maintenance application for a large set of Microsoft Word Autotexts |
| [PCon 3.5]({{"projects/pcon35" | absolute_url}}.html) | 1999 | Extension of the productivity-controlling software by a DCOM Locking-Server for concurrent user access |
| [KnowledgeNow 2]({{"projects/kn2" | absolute_url}}.html) | 1999 | An application for searching mutliple document types in heterogeneous backend environments |
| [CORBA/CGI Bridge]({{"projects/corbacgi" | absolute_url}}.html) | 1998 | A CORBA prototype application as a substitute for a CGI program |
| [COM2CORBA]({{"projects/com2corba" | absolute_url}}.html) | 1998 | Prototype application for the evaluation of a COM2CORBA bridge |
| [Publish C/S]({{"projects/publish" | absolute_url}}.html) | 1998 | A DCOM/ActiveX application for remote content distribution |
| [WebCam]({{"projects/webcam" | absolute_url}}.html) | 1998 | A patchwork web camera |
| [Seminar: Approaches for the integration of distributed computation into the WWW]({{"projects/seminar" | absolute_url}}.html) | 1998 | A presentation I gave at university |
| [Homepage]({{"projects/homepage" | absolute_url}}.html) | 1997 | My former home pages |
| [MRComputer]({{"projects/mrcomputer" | absolute_url}}.html) | 1997 | Order management for a small computer selling business |
| [PathEditor]({{"projects/patheditor" | absolute_url}}.html) | 1996 | A graphical user-interface for defining pathways of a small vehicle |
| [PCon 3.0]({{"projects/pcon30" | absolute_url}}.html) | 1995-1997 | Port of the productivity-controlling application to the Microsoft Foundation Classes |
| [PCon 1.2]({{"projects/pcon12" | absolute_url}}.html) | 1993-1995 | A productivity-controlling application for manufacturing businesses |
| [DOS International]({{"projects/dos" | absolute_url}}.html) | 1993 | An article for the computer magazine 'DOS International' about C++ templates |
| [Allowance]({{"projects/allowance" | absolute_url}}.html) | 1991-1993 | Allowance calculation on construction sites |
| [Energy Cost Calculation]({{"projects/eccalc" | absolute_url}}.html) | 1991 | Comparing energy costs of electric generators and electricity suppliers |
| [Telefax]({{"projects/fax" | absolute_url}}.html) | 1990-1991 | A fax template wrapper for a DOS fax application |
| [Offer]({{"projects/offer" | absolute_url}}.html) | 1990-1991 | Offer management for electric generators |
| [Amiga]({{"projects/amiga" | absolute_url}}.html) | 1986-1990 | Demos and some game tests for the Commodore AMIGA |
| [C64/128]({{"projects/c64128" | absolute_url}}.html) | 1985 | Several BASIC applications for the Commodore 64/128 | 

**NOTE: Unless otherwise stated, the software on this site is provided "as-is," without any express or implied warranty. In no event shall I be held liable for any damages arising from the use of the software.**
