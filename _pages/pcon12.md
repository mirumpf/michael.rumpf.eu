---
title:  "PCon 1.2"
layout: single
permalink: /projects/pcon12
author_profile: true
comments: false
---

An application for monitoring productivity in small to medium size businesses of the manufacturing industry.

The software was used to offer controlling services to customer companies, primarily regional shoe companies. The analysis of the collected productivity data was done on a daily, monthly, quarterly, and yearly basis and helped the customers to improve the productivity of their manufacturing processes.

[![Main Window]({{"/assets/images/pcon12mainsmall.png" | absolute_url}})]({{"/assets/images/pcon12main.png" | absolute_url}}) 
*Main Window*

The program is based on a number system to classify the different data collections. Encoded in the number system is the hierarchical structure of the company and department to be controlled. A 3-part number is assigned to all departments, where each part represents a level in the organizational structure.

[![Organization Number Assignment]({{"/assets/images/pcon12nummernorgsmall.png" | absolute_url}})]({{"/assets/images/pcon12nummernorg.png" | absolute_url}})
*Organization Number Assignment*

The next step is to create the actual departments in the hierarchy, created previously. Numbers are attached to actual department names.

[![Define Organization]({{"/assets/images/pcon12orgaufbausmall.png" | absolute_url}})]({{"/assets/images/pcon12orgaufbau.png" | absolute_url}})
*Define Organization*

Working tasks are classified into general, common, and supporting tasks. All tasks get a two-digit number assigned where common tasks get numbers between 80 and 89 and supporting tasks get numbers between 90 and 99. The general tasks have numbers from 00 to 79\. Those are defined later during the actual data collection phase.

[![Assign Task Numbers]({{"/assets/images/pcon12nummernarbsmall.png" | absolute_url}})]({{"/assets/images/pcon12nummernarb.png" | absolute_url}})
*Assign Task Numbers*

On the following screen employees are assigned to departments and their general working times are defined.

[![Department Base Data]({{"/assets/images/pcon12stammdatensmall.png" | absolute_url}})]({{"/assets/images/pcon12stammdaten.png" | absolute_url}})
*Department Base Data*

The following screens show the actual data collection interface. Data must be entered into these masks on a daily basis.

[![Daily Tasks]({{"/assets/images/pcon12tagesdatenarbeitsmall.png" | absolute_url}})]({{"/assets/images/pcon12tagesdatenarbeit.png" | absolute_url}})
*Daily Tasks*

[![Daily Task Types]({{"/assets/images/pcon12tagesdatenstuartsmall.png" | absolute_url}})]({{"/assets/images/pcon12tagesdatenstuart.png" | absolute_url}})
*Daily Task Types*

On the basis of the entered data the following reports are offered by the software:

[![Daily Productivity Tasks]({{"/assets/images/pcon12tagprodarbeitsmall.png" | absolute_url}})]({{"/assets/images/pcon12tagprodarbeit.png" | absolute_url}})
*Daily Productivity Tasks*

[![Daily Productivity Departments]({{"/assets/images/pcon12tagprodorganesmall.png" | absolute_url}})]({{"/assets/images/pcon12tagprodorgane.png" | absolute_url}})
*Daily Productivity Departments*


[![Productivity Times]({{"/assets/images/pcon12prodzeitsaldsmall.png" | absolute_url}})]({{"/assets/images/pcon12prodzeitsald.png" | absolute_url}})
*Productivity Times*

[![Productivity Tasks]({{"/assets/images/pcon12prodarbsmall.png" | absolute_url}})]({{"/assets/images/pcon12prodarb.png" | absolute_url}})
*Productivity Tasks*


[![Distribution Time Departments]({{"/assets/images/pcon12vertorgzeitsmall.png" | absolute_url}})]({{"/assets/images/pcon12vertorgzeit.png" | absolute_url}})
*Distribution Time Departments*

[![Distribution Activity Times]({{"/assets/images/pcon12vertorgzeitaktivsmall.png" | absolute_url}})]({{"/assets/images/pcon12vertorgzeitaktiv.png" | absolute_url}})
*Distribution Activity Times*


[![Amount Task Times]({{"/assets/images/pcon12mengarbsmall.png" | absolute_url}})]({{"/assets/images/pcon12mengarb.png" | absolute_url}})
*Amount Task Times*

[![Distribution Task Department]({{"/assets/images/pcon12vertarborgsmall.png" | absolute_url}})]({{"/assets/images/pcon12vertarborg.png" | absolute_url}})
*Distribution Task Department*



*   Daily Productivity Tasks  
    Productivity of all tasks of one department on one day, together with the monthly sums.
*   Daily Productivity Departments  
    Daily productivity for departments on one single day.
*   Productivity Times  
    Time based productivity over the specified period.
*   Productivity Tasks  
    Productivity of tasks over the defined period. For the average calculation only values inside the tolerance area were taken into account.
*   Distribution Time Departments  
    Distribution of the absence times of the employees over the defined period.
*   Distribution Activity Times  
    Distribution of activity times of the employees over the defined period.
*   Amount Task Times  
    Amount of task times over time.
*   Distribution Task Department  
    Distribution of tasks per department, restricted to the seven most frequent tasks of the department.

The macro features are used to combine several reports for display or printing.



[![Display Macros]({{"/assets/images/pcon12macros0small.png" | absolute_url}})]({{"/assets/images/pcon12macros0.png" | absolute_url}})  
*Display Macros*

[![Macro Definition]({{"/assets/images/pcon12macros1small.png" | absolute_url}})]({{"/assets/images/pcon12macros1.png" | absolute_url}})  
*Macro Definition*


[![Create Department Macro]({{"/assets/images/pcon12macros2small.png" | absolute_url}})]({{"/assets/images/pcon12macros2.png" | absolute_url}})  
*Create Department Macro*

[![Create Task Macro]({{"/assets/images/pcon12macros3small.png" | absolute_url}})]({{"/assets/images/pcon12macros3.png" | absolute_url}})  
*Create Task Macro*



|||
| --- | --- | 
| Type | Software Developer at ProCom GmbH | 
| Environment | Windows 3.1, Borland C++ 3.1, C++, OWL | 