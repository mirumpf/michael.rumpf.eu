---
title:  "Seminar"
layout: single
permalink: /projects/seminar
author_profile: true
comments: false
---

A presentation about 'Approaches for the integration of distributed computation into the WWW'

This paper gives an overview on different architectures for smoothly integrating distributed computation into the World Wide Web (WWW). Of the four approaches presented, three deal with the integration on a very basic level. The last one deals with higher level problems like referential integrity and migration transparency.

|||
| --- | --- | 
| Type | University Presentation | 
| Environment | Sun Solaris, LaTeX, pdfTeX, vi | 
| Link | [slides.pdf]({{"/assets/files/slides.pdf" | absolute_url}}) (287kB) [report.pdf]({{"/assets/files/report.pdf" | absolute_url}}) (430kB) | 