---
title:  "Twister"
layout: single
permalink: /projects/twister
author_profile: true
comments: false
---

An J2EE/C++ application server running on six different operating-system platforms.

In April 2000 I joined the Twister core team of the Brokat Server Technology Division (STD), where the J2EE Application Server "Twister" has been developed.

At that time the server was sold as an independent product. Unfortunately the decision was made in 2001 not to compete with the big players anymore but to use the server as a platform for our internal customers only. That was the state of the product until July 2004, when the final decision was made not to continue the development of the Twister J2EE application server but to move to the BEA WebLogic platform instead.

# Projects

The following list gives an overview on all the Twister related projects I have been working on.

## Twister 6.0 (Codename: NEO)
At the beginning of 2004 a final attempt was made to develop a new major version of Twister. The idea was to get rid of all C++ legacy components and to re-implement the functionality in Java.

In the Twister 5.x product line the following components were still implemented in C++:

*   Launcher
*   LoggingService
*   NamingService
*   LoadbalancinService

Support for RDOs and the LicenseService was not required anymore and thus could be dropped for Twister NEO.

### Launcher  
The Twister Launcher is responsible for starting the different processes and permanently checking them for availability. If an unavailable service is detected, the Launcher tries to kill the hanging process and eventually restarts it to bring the service back to life. This functionality is nearly impossible to implement in pure Java, so that a small native library was necessary to provide these process management features.

### LoggingService  
The C++ version of the LoggingService, like the NamingService, had many fault-tolerant features. The Twister version of the Java Logging API could detect an outage of the LoggingService and transparently switch to a fallback instance. When no service could be reached the client writes logging records into a local file and retransmits the records as soon as a LoggingService becomes available again. These key features had to be preserved for the pure java solution.

### NamingService  
In Twister NEO the fault-tolerant CORBA NamingService has been substituted by a NamingService interface directly in the Launcher. Each Service, started by the Launcher is registered in the service repository list and thus can be found by a client lookup. In the first version of the new NamingService, no fault-tolerance features were planned.

### LoadbalancingService  
The biggest challenge was the LoadbalancingService (LBS) wich is responsible for instantiating RDO or EJB objects. The intial request to an object is sent to the LBS which checks whether there is an instance of the object already instantiated and creates one if not. Objects live in containers and the old LBS had a sophisticated algorithm for distributing load over the configured amount of containers. The new, pure Java solution has a plugin mechanism for load-distribution algorithms. As a default a round-robin algorithm has been implemented, distributing the requests constantly over the different containers.

An alpha version of the services had already been implemented. Unfortunately the decision was made to completely stop the development because the risk to switch to this new Twister version for the different projects was considered to be too high.

## LoggingService - SNMP Trap Functionality
For the C++ LoggingService I have implemented a SNMP trap feature. A section in the configuration file `config.xml` contained a list of error id ranges for which a SNMP trap should be sent to an also configurable list of trap hosts. When the SNMP trap feature has been enabled and an error with an id from the range occurs, the logging service sends a SNMP trap to the list of trap hosts.

## JSR-77 - J2EE Management Specification
For Twister 5.3 I evaluated the J2EE Management Specification and implemented the Managed EJB (MEJB) from this specification for the distributed Twister service framework. This implementation is based on a ManagementService component which provides the MEJB via a RMI-IIOP interface. MBeans can be registered at the ManagementService's MEJB by configuration or programmatically.

The MBean hierarchy, as proposed by the [JSR-77](http://www.jcp.org/en/jsr/detail?id=77) specification, was planned for the next version of Twister and has not been implemented for Twister 5.3.

## EJB 2.0 Local Interfaces
For Twister 5.3 version I implemented EJB 2.0 Local Interface support for stateless and stateful session beans ([EJBLocalObject](http://java.sun.com/j2ee/sdk_1.3/techdocs/api/javax/ejb/EJBLocalObject.html), [EJBLocalHome](http://java.sun.com/j2ee/sdk_1.3/techdocs/api/javax/ejb/EJBLocalHome.html))

## Testing
For performance testing we used the J2EE test frameworks: [ECPerf](http://java.sun.com/j2ee/ecperf/index.jsp) and SPECjAppServer [2001](http://www.spec.org/jAppServer2001/)/[2002](http://www.spec.org/jAppServer2001/). I did some customizations on the latter ones to use them as a regression test suite that can be run automatically during nightly runs.

Version 1.3 of the J2EE Compliance Test Suite (CTS, only available to Sun [Java Partners](https://javapartner.sun.com)) had been used in an attempt to get the J2EE 1.3 compliance certification for the Twister application server. We tried to automate the J2EE CTS test runner to be able to use the test suite during nightly regression tests but unfortunately this was not possible with the 1.3 version of the CTS. The test runner had major flaws so that we were forced to split the whole test run into many little steps. We did not get it working reliably until the strict licensing conditions stopped us from proceeding. Version 1.4 had already been released and a licensee is required to finalize the licensing of one version at least 1 1/2 years after a new version has been released. Because of resource shortage we were not able to keep this deadline and thus gave up the certification.

## OpenORB
When the decision was made to use [OpenORB](http://openorb.sourceforge.net) as the basis for the Java components of the Twister application server, I started providing internal support for OpenORB related problems.

In October 2001 I got CVS write access to the OpenORB CVS server hosted at Exolab. Around December 2002 Steve McConnell and me decided to fork the project in order to get the community more involved in all decisions about the future of the project. We choose the name "The Community OpenORB" for the project and moved the sources to SourceForge. The current status of the community effort can be tracked on [SourceForge](http://openorb.sourceforge.net/). I stopped contributing to the project in December 2004 after it was clear that the Tiwster project is finally dead.

A list of all my bug reports to the original OpenORB project can be found [here](http://bugzilla.exolab.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=RESOLVED&bug_status=VERIFIED&bug_status=CLOSED&email1=rumpfonline&emailtype1=substring&emailassigned_to1=1&emailreporter1=1&emaillongdesc1=1&email2=&emailtype2=substring&emailreporter2=1&bugidtype=include&bug_id=&changedin=&votes=&chfieldfrom=&chfieldto=Now&chfieldvalue=&product=OpenORB&short_desc=&short_desc_type=substring&long_desc=&long_desc_type=substring&bug_file_loc=&bug_file_loc_type=substring&field0-0-0=noop&type0-0-0=noop&value0-0-0=&cmdtype=doit&order=Bug+Number) (70 bugs). My contributions to "The Community OpenORB" can be found in SourceForge CVS and on the mailing-list archives.

## Security
In Twister 4.x the C++ Secure Socket Layer (SSL) implementation was not fully standard compliant and caused incompatibilities when communicating with the standard-compliant Java Secure Socket Extension (JSSE) used by OpenORB's SSL module. My task was to make our C++ SSL legacy implementation fully compliant with OpenORB's SSL/JSSE implementation.

For Twister 5.3 I started a _Common Secure Interoperability Version 2_ (CSIv2) implementation in order to pass the interoperability category of the J2EE Compliance Test Suite (CTS). This project had been stopped when it turned out that the J2EE certification was no longer a goal. A first draft of the implementation can be found [here](http://cvs.sourceforge.net/viewcvs.py/openorb/SSL/src/main/org/openorb/orb/csiv2/).

## Build Process
During the evolution of the Twister 4.x/5.x versions the software's core functionality started spreading over many different CVS modules. As a consequence the maintenance became very time-consuming because of the dependencies. At that time I created a so-called Twister Meta-Project (TwiMP) which wraps the different CVS modules into one, Ant based, project, creating the impression that there is only one pure Java module, hiding the C++ legacy components behind the Ant build script.

For the versions Twister 5.3 and Twister 6 we used a customized version of our internal [CruiseControl](http://cruisecontrol.sourceforge.net) System to achieve continuous build integration on a TwiMP instance. Part of the customization was to make the tools like findbugs, pmd, and checkstyle work on a TwiMP instance to get the code quality, test-coverage, and bug-pattern detection reports for the Twister source-code.

## Fault-tolerant NameService and JNDI Service Provider
In Twister the CORBA NameService is an integral part of the application server. The Twister 4.0 implementation provides a fault tolerance mechanism that makes the service violate the CORBA NameService specification. The goal for Twister 5.0 was to modify the NameService to make it compliant to the OMG specification and at the same time to keep the fault tolerance features.

The former NameService was written in C++. All bindings are stored in the root context and creating sub-contexts was not possible. For the new version I chose the approach to implement the CORBA IDL interface, defined by the NameService 1.2 specification and to provide the fault tolerance mechanisms via an IDL interface extension.

The "CosNamingExtFT" extension interface is introduced for creating and manipulating fault-tolerant contexts and bindings. A context can be created in normal mode and in fault-tolerant mode. In fault-tolerant mode CORBA object instances, running on multiple nodes, can be bound under one name in a fault-tolerant context. Upon look-up the NameService service tries to return the entry for the local node first. If there is no local instance available, the first from the list is returned. The NameService checks with a "ping" invocation whether the instance to be returned is available. If this check fails, another entry from the sub-context is pinged. Only if no object can be found a NameNotFound exception is thrown.

Additionally a CosNaming Service Provider Interface (SPI) for the Java Naming Directory Interface (JNDI) has been written. This wrapper allows Java clients to access objects in the NameService via normal JNDI lookup mechanisms.

## Base Toolkit
The base toolkit is the API for RDOs (Repository Defined Objects), the Twister-proprietary component model, which is used by application developers to interact with the application server. This API is available in the Twister 4.x and 5.x versions.

The application programming interface has been designed with simplicity and consistency in mind. The goal was to reduce the differences between the C++ and the Java programming languages. A developer switching between the two languages should not see any major differences, besides the language inherent ones. This was especially important for many projects that were moving from C++ to Java. In those projects many legacy components were still written in C++, whereas new components have already been developed in Java.

## Examples
The implementation of examples for the 4.0 version of the Twister application server was my first task on the new job. This gave me the opportunity to start learning about the application server internals.

The examples were delivered with the 4.0 version of the product and served as a starting point for application developers, developing projects on top of the Twister application server.

The examples are based on the factory pattern and use the classical Account scenario. The client applications and server objects are developed in all Twister supported programming languages (Java, C++, TCL) and component models (CORBA, EJB, RDO).

|||
| --- | --- | 
| Type | Junior Software Engineer to Senior Software Arcchitect at Brokat/Encorus Technologies/First Data Mobile Solutions | 
| Environment | TODO | 
