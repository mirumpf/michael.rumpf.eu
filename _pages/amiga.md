---
title:  "Amiga"
layout: single
permalink: /projects/amiga
author_profile: true
comments: false
---

Several fun programs written in [68000 Assembler](https://en.wikipedia.org/wiki/Motorola_68000) on the [Commodore Amiga](https://en.wikipedia.org/wiki/Amiga). 
The purpose of those applications was just to explore the AMIGA hardware capabilities and to have fun.

The programs below have been tested on the [Amiga Emulator UAE](http://www.amigaemulator.org/) version 0.6.9 R12.

# Misc
The small test application below moves five sprites and draws some lines with help from the processor, that means no Amiga custom chips are involved in this test.

[![Five hopping sprites]({{"/assets/images/5imageshuepflinessmall.png" | absolute_url}})]({{"/assets/images/5imageshuepflines.png" | absolute_url}})

# Vector
The first screenshot shows my first working vector graphics algorithm.

The second screenshot shows a little demo application in which I combined the [Copper](https://en.wikipedia.org/wiki/Original_Chip_Set#Copper) custom chip, a three-layered star scrolling, and the previously mentioned vector graphics algorithm.

[![vector]({{"/assets/images/vectorsmall.png" | absolute_url}})]({{"/assets/images/vector.png" | absolute_url}}) [![vector2]({{"/assets/images/vector2small.png" | absolute_url}})]({{"/assets/images/vector2.png" | absolute_url}})

[vector.tar.bz2]({{"/assets/files/vector.tar.bz2" | absolute_url}}) (38kB)

# Demo
In the next screenshot you can see my first fully working Amiga demo applicatin with three moving bars ([Copper](https://en.wikipedia.org/wiki/Original_Chip_Set#Copper)), a star layer scrolling (Processor), a scrolling text line ([Blitter](https://en.wikipedia.org/wiki/Original_Chip_Set#Blitter)), together with sound.

[![The first Amiga Demo]({{"/assets/images/demosmall.png" | absolute_url}})]({{"/assets/images/demo.png" | absolute_url}})

[YouTube](https://youtu.be/hugUBPNy510) -- [demo.tar.bz2]({{"/assets/files/demo.tar.bz2" | absolute_url}}) (68kB)

# MegaDemo II
Here you can see a part of a so called Mega-Demo. Unfortunately the other members of our small computer club were not so enthusiastic about writing a mega-demo and so my part was the only part that has ever been finished ;)

The demo contains the usual star scrolling, two rotating color lines ([Copper](https://en.wikipedia.org/wiki/Original_Chip_Set#Copper)), a scroll line ([Blitter](https://en.wikipedia.org/wiki/Original_Chip_Set#Blitter)), a rotating logo ([Blitter](https://en.wikipedia.org/wiki/Original_Chip_Set#Blitter)), four sprites flying around, together with sound.

[![MegaDemo II]({{"/assets/images/megademo2small.png" | absolute_url}})]({{"/assets/images/megademo2.png" | absolute_url}})

[YouTube](https://youtu.be/Pa3Nz7zhJeg) -- [megademo2.tar.bz2]({{"/assets/files/megademo2.tar.bz2" | absolute_url}}) (190kB)

# Game
This was my first attempt to write a game on the Amiga. The bottom stripes are moving and thus creating the impression that the airplane is flying over the plane.

[![Game]({{"/assets/images/gamesmall.png" | absolute_url}})]({{"/assets/images/game.png" | absolute_url}})

[game.tar.bz2]({{"/assets/files/game.tar.bz2" | absolute_url}}) (35kB)

# Coder
This program should be used for encrypting floppy disks in order to make their contents accessible by password only.

The GUI is non-standard, which means that no AmigaOS GUI libraries are used. Loading directories is implemented, the GUI fully operational, but the actual encryption algorithms had never been finalized.

[![coder]({{"/assets/images/codersmall.png" | absolute_url}})]({{"/assets/images/coder.png" | absolute_url}})

[coder.tar.bz2]({{"/assets/files/coder.tar.bz2" | absolute_url}}) (60kB)

# Seek & Destroy (S&D)
This game is a classical fly-n-shoot game. Unfortunately the version I found on the Amiga disks is broken so that the actual game could not be started anymore, only the menus shown here are still working.

Steering the helicopter over a landscape was already working, as well as firing rockets, but the full game was never finalized.

[![sad1]({{"/assets/images/sad1_small.png" | absolute_url}})]({{"/assets/images/sad1.png" | absolute_url}}) [![sad5]({{"/assets/images/sad5_small.png" | absolute_url}})]({{"/assets/images/sad5.png" | absolute_url}}) [![sad6]({{"/assets/images/sad6_small.png" | absolute_url}})]({{"/assets/images/sad6.png" | absolute_url}})

[![sad7]({{"/assets/images/sad7_small.png" | absolute_url}})]({{"/assets/images/sad7.png" | absolute_url}}) [![sad8]({{"/assets/images/sad8_small.png" | absolute_url}})]({{"/assets/images/sad8.png" | absolute_url}}) [![sad2]({{"/assets/images/sad2_small.png" | absolute_url}})]({{"/assets/images/sad2.png" | absolute_url}})

[![sad3]({{"/assets/images/sad3_small.png" | absolute_url}})]({{"/assets/images/sad3.png" | absolute_url}}) [![sad4]({{"/assets/images/sad4_small.png" | absolute_url}})]({{"/assets/images/sad4.png" | absolute_url}}) [![sad9]({{"/assets/images/sad9_small.png" | absolute_url}})]({{"/assets/images/sad9.png" | absolute_url}})

[seekndestroy.tar.bz2]({{"/assets/files/seekndestroy.tar.bz2" | absolute_url}}) (378kB, ADF)

|||
| --- | --- | 
| Role | Architect, Developer | 
| Environment | AMIGA OS, SEKA 68000 Assembler 3.2, Deluxe Paint 4 | 