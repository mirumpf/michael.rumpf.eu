---
title:  "About"
layout: single
permalink: /about/
author_profile: true
comments: false
---

This website provides information about my working experience, especially about the software projects I've been involved with since I started with computers back in 1982.

My fascination for computers started in 1982 with a Sinclair [ZX-81](https://en.wikipedia.org/wiki/ZX81). After using several home computers like the [TI-99 4/A](https://en.wikipedia.org/wiki/Texas_Instruments_TI-99/4A) and the [VIC-20](https://en.wikipedia.org/wiki/Commodore_VIC-20), I wrote my first BASIC program on a [C64](https://en.wikipedia.org/wiki/Commodore_64) in 1985. I learned 8-Bit Assembler on the [C128](https://en.wikipedia.org/wiki/Commodore_128) and later M68000 Assembler and C on the [Commodore Amiga](https://en.wikipedia.org/wiki/Amiga), until I finally turned away from home computers to writing software in Turbo Pascal on the PC/MS-DOS platform. From that point on I worked on many different Windows and UNIX platforms, using C/C++ until I turned to Java as my main programming language around the year 2000.

My diploma thesis in 1998 initiated a strong interest in distributed systems. This was a big asset during my engagement with Brokat/Encorus/First Data, where I had the chance to work at the heart of the Brokat Java Enterprise Edition application server Twister, which was used by many banks to build their online banking solution.

Since the late nineties I did different contributions to various open-source and free software projects.

## MEMBERSHIPS

### Active

*   [GitLab](https://gitlab.com/mirumpf) (Since April 2018)
*   [GitHub](https://github.com/mrumpf) (Since November 2011) 
*   [Stackoverflow](https://stackoverflow.com/users/1518964/michael-rumpf) (Since July 2012)
*   [LinkedIn Member](https://www.linkedin.com/in/michael-rumpf-580a23105/) (Since April 2008)
*   [Xing Member](http://www.xing.com/profile/Michael_Rumpf2) (Since June 2005)
*   [Java User Group Stuttgart](http://www.jugs.de/) (JUGS) Member (Since June 2004)

### Inactive

*   [Jenkins](https://plugins.jenkins.io/repository-connector) (July 2014 until March 2018)
*   [OpenORB](http://openorb.sourceforge.net/) -- Committer (January 2002 until 2005)
*   [ORBit Resource](https://web.archive.org/web/20170815195940/http://orbit-resource.sourceforge.net/) -- Maintainer (August 2000 until 2003)
*   [Encorus Software Architecture Group](http://www.jcoderz.org) (SWAG) -- Member (March 2003 until January 2006)


## PUBLICATIONS

*   [Weblog]({{"/" | absolute_url}}) (Since June 2005)
*   [Diploma thesis]({{"/projects/kn2" | absolute_url}}) (2000)
*   [Approaches for the integration of distributed computation into the WWW]({{"/projects/seminar" | absolute_url}}) (1998)
*   [Article for 'DOS International']({{"/projects/dos" | absolute_url}}) (1993)

## TECHNOLOGIES

| | |
| --- | --- |
| Cloud Native | Kubernetes, Cloud Foundry, Istio, Jäger/OpenTracing, Ambassador, Fluentd, Docker, Helm, HAProxy, Nginx, Buildpacks, OpenServiceBroker |
| Spring Frameworks | Spring Framework, Spring Boot, Spring Data |
| Netflix Components | Eureka, Zuul, Feign, Hystrix, Ribbon |
| Clouds | IBM Cloud, Google Cloud, Azure, AWS, Open Telekom Cloud (OTC) | 
| Languages | Java, Kotlin, C/C++, XML, HTML/XHTML, JavaScript, SQL, Microsoft VisualBasic (VBA), Bash, Perl, Python/Jython, Pascal, 6502/68000 Assembler | 
| Debugger | gdb, ccc, Eclipse | 
| Compiler | javac, gcc, Microsoft Visual C++, Borland C++, Sun C++, IBM C++, HP Cxx | 
| Continuous Integration Tools | GitLab, Concourse CI, Hudson/Jenkins, CruiseControl, CruiseControl .NET | 
| Monitoring | Dynatrace, AppDynamics, Elasticsearch/Kibana, Prometheus | 
| Web-Technologies | REST, JSP, Servlets, CSS, AJAX, JavaScript, HTML/XHTML, JSTL, Java WebStart | 
| Web-Frameworks | Angular, JSF (Myfaces, ICEfaces), Struts, Wicket, Quasar (Daimler internal) | 
| Web-Services | REST, SOAP, Axis (+Castor) | 
| Server-Frameworks | Spring Boot/Cloud, JEE, Pro Active Infrastructure (PAI, Daimler internal) | 
| CORBA-Implementations | OpenORB, Sun ORB, VissBroker, ORBix, ORBix COMet, ORBit, TAO | 
| Reporting Tools | Crystal Reports, Jasper Reports, ADF Reporting (Aspose-Slides, -Words, -Cells, and -Pdf) | 
| Messaging | RabbitMQ (AMQP), ActiveMQ (JMS), IBM WebSphere MQ | 
| Microsoft Technologies | ActiveX, COM, DDE | 
| XML-Technologies | XML, XSL, JaxB, Castor, XPath | 
| OR-Mapper | Hibernate, JPA | 
| Rich Client Frameworks | Eclipse Rich Client Platform, Swing, Microsoft Foundation Classes, Borland OWL, Turbo-Vision, XWindows | 
| Issue Tracking | GitLab, GitHub, JIRA, Bugzilla, Trac, Google Code Hosting, Sourceforge | 
| Test-Tools | JUnit, JUnitPerf, JMeter, Selenium | 
| Build-Tools | Gradle, Maven, Ant, make, msbuild | 
| Code-Anaylsis | SonarQube, jQAssistant, Checkstyle, Findbugs, PMD, JCoderz Report, JDepend | 
| IDE | IntellijIDEA, Eclipse, IBM WSAD/RAD, Microsoft Visual Studio. Microsoft Visual Basic, Netbeans, Borland C++, Turbo Pascal, SEKA Assembler | 
| VCS | Git, Subversion, CVS, Microsoft Team-Foundation Server, Clearcase | 
| DBMS | PostgreSQL, MySQL, H2/HSQL-DB, JavaDB/Derby, Oracle, MS-SQL, Microsoft Access | 
| Modelling-Tools | PlantUML, Visual Paradigm, Enterprise Architect, StarUML, Visio | 
| Methodologies | Domain-Driven Design (DDD), OOA / OOD / OOP, TDD, XP, Agile, MDA/MDSD | 
| Wikis | GitLab, GitHub, Confluence, MediaWiki, Plone, TikiWiki | 
| Diagrams | UML, ER, Flow | 
| Protocols | gRPC, HTTP, IIOP, TCP/IP | 
| Domains | Automotive, Software, Finance, Telecom | 
| Applications | Asciidoc, Microsoft Office, OpenOffice |
