---
title:  "Achievo"
layout: single
permalink: /projects/achievo
author_profile: true
comments: false
---

My role in [Achievo Deutschland AG](https://web.archive.org/web/20100108110134/http://www.achievo.de/) can best be described with the term _Technical Lead Software Development_. I had basically the following responsibilities:

*   Development Process (Offshore and Nearshore)
*   Software Development Guidelines
*   Definition and Compliance Check of Quality Gates
*   Technical Architecture
*   Coaching of new Developers
*   Build Process and Continuous Integration Strategy
*   WebSphere Application Server Configuration and Deployment

For the customer projects my role is that of a senior software architect.

# TACO - Transparent Analysis and Cost Optimization

The TACO project is a J2EE application which supports the purchasing process of car components for an automotive manufacturer.

# DFM - Designed for Manufacturing (PPG - Produktionsgerechte ProduktGestaltung)

The DFM project is a J2EE application which supports the optimization process during the production of a new car model.

# ADF - Achievo Development Framework

ADF is a framework which provides common functionality to the Achievo Java/J2EE projects.

|  |  |
|---|----|
| Role | Senior Software Architect and Technical Lead Software Development |
| Environment | Windows, Linux, Solaris, Eclipse, IBM WebSphere 5.0/6.0/6.1, Java 1.4/1.5, J2EE1.2/1.3/1.4, Struts 1.1, JSF 1.1, CruiseControl/Hudson, Findbugs, Checkstyle, PMD, Oracle 9.x-10.x, Aspose Slides/Words/Cells/Pdf, Pro Active Infrastructure (PAI) 3.x - 4.0 |

