---
title:  "WebCam"
layout: single
permalink: /projects/webcam
author_profile: true
comments: false
---

A webcam based on AMIGA M-JPEG digitizer hardware and a collection of scripts to upload the images to an FTP account.

[![Main Window]({{"/assets/images/webcamuploader_small.png" | absolute_url}})]({{"/assets/images/webcamuploader.png" | absolute_url}})
*Main Window*

This project was an attempt to create a WebCam with existing hardware, because a professional WebCam was very expensive at that time.

*   Hi8 video camera
*   Amiga 4000
*   VLab Motion, a Motion-JPEG digitizer card
*   Parallel port connection (PC2Amiga) to the PC
*   Windows PC
*   AOL '50 hours free' internet access

The video camera is attached to the digitizer board on the Amiga. The digitizer software is controlled by an ARexx script that takes pictures every five seconds and sends them with the PC2Amiga client over the parallel port to the PC. On the PC a VisualBasic application, using the Inet ActiveX control, uploads the file from the PC to an Internet FTP account. On the server, where the picture is uploaded to, a Cron script is running that moves the file to a folder on a web server, where it can be viewed by an internet browser.

Besides partially uploaded files and several restarts of the various scripts the WebCam was working quite nicely.

|||
| --- | --- | 
| Type | Personal Project | 
| Environment | Visual Basic 5, Bash, Amiga Shell, ARexx, FTP, Windows 9x/NT4, A4000 with VLab-Motion and Movieshop 3.x | 
| Link | [WebCam tarball]({{"/assets/files/webcam.tar.bz2" | absolute_url}}) (10kB) (VB Source-Code) [pc2am tarball]({{"/assets/files/pc2am.tar.bz2" | absolute_url}}) (106kB) (PC2Amiga208.lha and PC2AmFix.lha) [WebCam archive]({{"/assets/files/webcam.lha" | absolute_url}}) (95kB) (ARexx-Script and Batch-File) | 