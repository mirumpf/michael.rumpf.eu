---
title:  "IBM"
layout: single
permalink: /projects/ibm
author_profile: true
comments: false
---

During my time at IBM worked on the following projects:

# Daimler Vehicle Backend

In June 2018 I started as the migration lead to move all microservices from the traditional data center to a modern cloud based environment. The most important objective was to move each microservice without an impact for the customers. A sophisticated Netflix Eureka synchronization mechanism has been established to make it seamless for the microservices, whether their peers are still running in the data center or have already been moved to the cloud.  
This also includes a bridge for mirroring messaging middleware traffic from the data center to several cloud based solutions.

|||
| --- | --- | 
| Role | Senior Software Architect | 
| Environments | Mac OSX, Linux, IntellijIDEA, Maven, Gradle, Java 8, Jenkins, Artifactory, SonarQube, Spring, Spring Boot, Spring Cloud, Kubernetes, IBM Cloud, ActiveMQ/RabbitMQ, PostgreSQL, Spring Integration | 