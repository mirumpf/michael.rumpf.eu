---
title:  "Publish C/S"
layout: single
permalink: /projects/publish
author_profile: true
comments: false
---

Distribution of documents into a remotely hosted web application.

The goal of this application was to provide an administration interface to a web-based information database for expatriates. The administrative interface was used to upload new content to the server systems, hosted in Chicago, Illionois, USA.

[![CountryNet Manager Architecture]({{"/assets/images/publish_small.png" | absolute_url}})]({{"/assets/images/publish.png" | absolute_url}})
*CountryNet Manager Architecture*

The information that is presented by the application comes from SGML documents that have been indexed by a Fulcrum SearchServer. In order to update the SGML documents an administration interface, called _CountryNet Manager_, has been developed.

[![CountryNet Manager]({{"/assets/images/countrynetmgrsmall.png" | absolute_url}})]({{"/assets/images/countrynetmgr.png" | absolute_url}})
*CountryNet Manager*

Distributed COM (DCOM) and an ActiveX control is used as distributed communication mechanisms. The ActiveX control _PublishClt_ accesses the _ProjectSrv_ and the _PublishSrv_ components in order to manage _projects_, the administrative units of the CountryNet Manager application.

![Projects Dialog]({{"/assets/images/publishctrlprj.png" | absolute_url}})  
*Projects Dialog*

The _PublishSrv_ provides functions for uploading and downloading files into a so called project. The _PublishSrv_ instantiates the _IndexFeedSrv_ component to which it feeds the uploaded files for index creation. The result is an index, created by the Fulcrum SearchServer, which is then fed to the _DBFeedSrv_ component for storing it into the database and making it available to the application for searching.

The reason for making extensive usage of DCOM components was to get used to this new technology and to identify any hurdles in using it for remote communication.

One lesson learned was that DCOM is making usage of distributed reference counting. A mechanism for keeping track of how many clients are accessing a server component. The problem with this approach is when a client crashes there is no way to tell the server to decrease its counter. The consequence of this is that the server component can not be garbage collected because the DCOM instantiation layer still thinks that the component is in use. The only way to fix this is to kill the component on the server manually.

[![ActiveX Control]({{"/assets/images/publishctrlsmall.png" | absolute_url}})]({{"/assets/images/publishctrl.png" | absolute_url}})
*ActiveX Control*

|||
| --- | --- | 
| Type | Software Developer at Arthur Andersen Software & Methods | 
| Environment | Windows 9x/NT4, Visual C++ 5 | 