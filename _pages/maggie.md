---
title:  "Micro Aggregation Engine (MAGGIE)"
layout: single
permalink: /projects/maggie
author_profile: true
comments: false
---

The MAGGIE prototype has been developed as a showcase for the New York public transport agency on how RFID based credit card payment could be leveraged for buying subway tickets.

The **M**icro **AGG**regat**I**on **E**ngine prototype has been developed for demonstrating the concept of aggregating small transaction amounts to larger values before they are settled against a traditional payment backend like a credit card or a bank account. Such a system is useful for markets with very low transaction amounts for which immediate settlement fees would be higher than the actual transaction value.

The first screen shows the start page. From here the user can enter the shopping area, and the consumer or merchant self-administration.

[![Start Page]({{"/assets/images/maggie1small.png" | absolute_url}})]({{"/assets/images/maggie1.png" | absolute_url}})  
*Start Page*

One of the shops in the shopping area is a prototype of how the New York public transport (MTA - Metropolication Transport Agency) could establish a system of buying subway tickets with the new RFID credit cards. The user swipes the RFID credit card over the card reader. The card is charged with the ticket price and the user is granted access to the train station. This functionality can be demonstrated by dragging one of the credit cards over the picture. The green display between the credit cards and the picture of the turnstile shows the result of the transaction.

[![New York MTA Shop]({{"/assets/images/maggie3small.png" | absolute_url}})]({{"/assets/images/maggie3.png" | absolute_url}})  
*New York MTA Shop*

In the ringtone shop the user can buy ringtones for his mobile phone.

[![Ringtone Shop]({{"/assets/images/maggie4small.png" | absolute_url}})]({{"/assets/images/maggie4.png" | absolute_url}})  
*Ringtone Shop*

When a ringtone has been selected the user must provide its payment data. In this case a credit card is the only accepted payment instrument.

[![Customer Payment Data]({{"/assets/images/maggie5small.png" | absolute_url}})]({{"/assets/images/maggie5.png" | absolute_url}})  
*Customer Payment Data*

In this prototype no SMS with the ringtone is sent, but the user gets a link from which he can download the ringtone.

[![Ringtone Download]({{"/assets/images/maggie6small.png" | absolute_url}})]({{"/assets/images/maggie6.png" | absolute_url}})  
*Ringtone Download*

In the following screen the consumer enters its authentication data in order to view his transaction history.

[![Consumer Self Administration - Authentication]({{"/assets/images/maggie7small.png" | absolute_url}})]({{"/assets/images/maggie7.png" | absolute_url}})  
*Consumer Self Administration - Authentication*

In the transaction view the consumer can check his personal transactions. The picture below shows those that have been aggregated until the amount of 7 USD was reached as settled because the credit card has already been charged at that time. The last transaction is in the state aggregated and will be settled later.

[![Consumer Self Administration - Transaction View]({{"/assets/images/maggie8small.png" | absolute_url}})]({{"/assets/images/maggie8.png" | absolute_url}})  
*Consumer Self Administration - Transaction View*

A similar view is available for merchants which shows the transactions of all consumers.

[![Merchant Self Administration - Transaction View]({{"/assets/images/maggie9small.png" | absolute_url}})]({{"/assets/images/maggie9.png" | absolute_url}})  
*Merchant Self Administration - Transaction View*

A merchant can define individually when settlements should take place. He can define so called aggregation rules which allow him to set an amount level at which the settlement should be executed. In this case the amount is set to 7 USD. That means that as soon as aggregated amounts reach the sum of 7 USD a settlement will be initiated.

[![Merchant Self Administration - Aggregation Rules]({{"/assets/images/maggie10small.png" | absolute_url}})]({{"/assets/images/maggie10.png" | absolute_url}})  
*Aggregation Rules*

AJAX (Asynchronous JavaScript And XML) is used by the subway ticket show-case to send the payment request to the server and receive the response without having to do a request/response round-trip. The user-interface is based on the JSF technology.

|||
| --- | --- | 
| Type | Senior Software Architect at First Data Mobile Solutions | 
| Environment | Linux, AIX, Eclipse, AJAX, JSF, JSP, XHTML, CSS | 