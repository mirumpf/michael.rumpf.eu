---
title:  "CORBA 2.2 Validator"
layout: single
permalink: /projects/validator
author_profile: true
comments: false
---

A perl script that runs positive and negative test-cases against arbitrary IDL compilers for specification compliance checking.

While learning CORBA I wrote some IDL files and a script for checking compliance of an IDL parser.

The files are separated into categories of valid and invalid IDL fragments. A Perl script grabs the different IDL files and feeds them to the IDL compiler. When the compiler stops while compiling a valid file or does not stop when compiling an invalid file then the script terminates and reports that it found an incompatibility of the IDL under test.

The related [CORVAL](http://www.opengroup.org/corval/) project of the Open Group tries to define a framework with which CORBA products can be checked for standard compliance.

|||
| --- | --- | 
| Type | Personal Project | 
| Environment | Linux, Perl, ORBit, GNU make | 
| Link | [validator.tar.bz2]({{"/assets/files/validator.tar.bz2" | absolute_url}}) (8kB) | 