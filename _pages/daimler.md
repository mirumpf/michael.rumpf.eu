---
title:  "Daimler"
layout: single
permalink: /projects/daimler
author_profile: true
comments: false
---

# Daimler Vehicle Backend


|||
| --- | --- | 
| Role | Senior Software Architect | 
| Environments | Mac OSX, Linux, IntellijIDEA, Maven, Gradle, Java 8, Jenkins, Artifactory, SonarQube, Spring, Spring Boot, Spring Cloud, Kubernetes, IBM Cloud, ActiveMQ/RabbitMQ, PostgreSQL, Spring Integration, Dynatrace, AppDynamics, Microsoft Azure | 
