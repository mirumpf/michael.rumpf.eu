---
title:  "Knowledge Now 2"
layout: single
permalink: /projects/kn2
author_profile: true
comments: false
---

The diploma thesis project is mostly about CORBA and distributed components. The thesis paper is available as PDF and can be downloaded from below.

This is my diploma thesis project. The full documentation is provided by the PDF below, but unfortunately the document is available in german only.

The diploma thesis has been written in LaTeX and was converted to a fully featured PDF document with pdfTeX, including links, page preview thumbnails, and contents overview.

While working on the project the following smaller spin-off projects have been written:

*   **DDEExecute**  
    The DDEExecute application is used to issue [Dynamic Data Exchange](http://msdn.microsoft.com/library/default.asp?url=/library/en-us/winui/winui/windowsuserinterface/dataexchange/dynamicdataexchange.asp) (DDE) commands from the command-line to applications supporting this Windows API.
*   **Launcher**  
    The launcher application is able to start the Windows default browser, registered in the Windows registry, and to send control DDE commands to a running instance.
*   **JavaRT**  
    JavaRT stands for 'Java Run Time' and is used to simplify start-up of Java applications. The executable tries to find installed Java VMs and executes the Java application with the VM found during the search. For customization the executable `JavaRT.exe` could be renamed to `MyApp.exe` together with an INI File where command line parameters like PATH, CLASSPATH, Class name, or other VM arguments for the application can be specified. JavaRT uses these settings to assemble the full command line for the application to be started. Any parameter to the JavaRT executable is passed as application parameter to the Java application.
*   **GrpSpawn**  
    GrpSpawn is a workaround to a Windows problem, not being able to kill a process tree from external, like it is possible on UNIX systems.  

    Only processes from inside the process tree, created with the flag CREATE_PROCESS_GROUP can be killed. The way to achieve this functionality is to install a signal handler in a process inside the process tree and wait for a termination signal from the outside. Upon reception of this signal, the process terminates the root of the process group, causing all child processes to die. Such an application is for example used by Microsoft Visual Studio when starting the build of a project. To be able to terminate the build at any time such a wrapper application is used.
*   **Installer**  
    The installer is an InstallShield clone written in VisualBasic. A USER module is available where installer routines can be placed and called from the installer's main loop in order to perform user-specific tasks. The installer is used for installing KN2's application server and the associated development environment.

|||
| --- | --- | 
| Type | Junior Software Engineer at Arthur Andersen Software & Methods | 
| Environment | Windows NT4/2000, Sun Solaris 6, Visual Studio 6 SP3, Sun C++ 4.2, GNU tool chain, CVS | 
| Link | [diploma thesis PDF]({{"/assets/files/diplomathesis.pdf" | absolute_url}}) (1.1MB) [diploma thesis source tarball]({{"/assets/files/diplomathesis-src.tar.bz2" | absolute_url}}) (2.2MB) | 