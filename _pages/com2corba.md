---
title:  "COM2CORBA"
layout: single
permalink: /projects/com2corba
author_profile: true
comments: false
---

An evaluation prototype testing CORBA access from VisualBasic applications.

The goal of this prototype was to evaluate a commercial available COM2CORBA bridge whether it is suitable to access CORBA objects from a Microsoft COM application.

The prototype has been developed with an evaluation version of Orbix 2.3 for Windows NT, Sun Solaris, and the IONA Orbix COMet COM2CORBA bridge.

[![COM2CORBA prototype archicteture]({{"/assets/images/com2corba_small.png" | absolute_url}})]({{"/assets/images/com2corba.png" | absolute_url}})  
*COM2CORBA prototype archicteture*

The server component runs under Solaris 5 and provides access to a Fulcrum Search Server, which is a SGML indexing engine. The client is a VisualBasic search application, running under Windows NT.

A search term is transparently mapped from COM into a CORBA request by the COM2CORBA bridge and send via IIOP to the CORBA server component. The component performs a search for the keyword with the Fulcrum Search Server before it returns the result list back to the VisualBasic client. The client finally displays the search results in a list to the user.

This prototype is a demonstration about how smoothly CORBA objects can be accessed via COM interfaces.

| | |
| --- | --- | 
| Type | Internship, Evaluation Project | 
| Environment | Windows 9x/NT4, Sun Solaris 5, Orbix COMet, SUN C++ 4.2, Visual Basic 5.0 | 