---
title:  "Offer"
layout: single
permalink: /projects/offer
author_profile: true
comments: false
---

This solution substituted an old and hard to use host-based application for writing offers for electrical power generators with a much simpler application interface.

This application has been developed to support a sales department in selling electrical generators and stand-by-sets.

The user chooses a generator model and selects various components from a list. This selection was mandated by requirements, defined by the customer.

[![Main Menu]({{"/assets/images/angebot3small.png" | absolute_url}})]({{"/assets/images/angebot3.png" | absolute_url}})  
*Main Menu*

The menu item 'Create Offer' leads the user to a screen where the selection of the generator base model and the optional components takes place.

The second item 'Print Complete List' dumps the previous selection to an attached printer.

The third item 'Edit Data' is used to modify the selection.

'Add Generator', the fourth menu item, offers an administrative feature for importing more generators to the internal database.

The fifth item is used to print the customer database to the screen or an attached printer.

[![Component List]({{"/assets/images/angebot1small.png" | absolute_url}})]({{"/assets/images/angebot1.png" | absolute_url}})  
*Component List*

On this screen components and generator base models are maintained.

[![Customer Data]({{"/assets/images/angebot2small.png" | absolute_url}})]({{"/assets/images/angebot2.png" | absolute_url}})  
*Customer Data*

This mask collects customer data to be used for an offer.

[![Optional Components]({{"/assets/images/angebot4small.png" | absolute_url}})]({{"/assets/images/angebot4.png" | absolute_url}})  
*Optional Components*

This screenshot shows the selection of optional components for a generator.

|||
| --- | --- | 
| Type | Freelancer Project | 
| Environment | MS-DOS, Turbo Pascal 4.0 | 
| Link | [offer.tar.bz2]({{"/assets/files/offer.tar.bz2" | absolute_url}}) (39kB) | 