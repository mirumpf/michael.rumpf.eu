---
title:  "Autotexts"
layout: single
permalink: /projects/autotexts
author_profile: true
comments: false
---

This project was a migration of WordBasic Macros to VisualBasic for Applications (VBA).

The company for which this program was developed was using a huge set of Microsoft Word Autotext fragments to generate offers for their customers. Each autotext fragment described one of their products, an offer was just a compilation of different fragments into one Word document.

The original version of the program contained a set of WordBasic scripts that inserted all the Autotexts into a document for editing the product descriptions. My task was to convert the script from WordBasic into a VisualBasic for Applications (VBA) program.

The problem with storing product descriptions as Autotexts is that this approach does not scale, that means it is not suitable for concurrent access, and the text fragments are very hard to maintain. As a solution to those problems, I wrote a prototype in Visual Basic, called [Inventory Management 1.0]({{"/projects/inventory" | absolute_url}}), which had been presented to the company. Unfortunately they chose an already established application instead.

| | |
| --- | --- | 
| Role | Architect, Developer | 
| Environment | Windows, Microsoft Word 97, VisualBasic for Applications | 
| Link | [autotexts.tar.bz2]({{"/assets/files/autotexts.tar.bz2" | absolute_url}}) (620kB) |
