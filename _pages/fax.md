---
title:  "Telefax"
layout: single
permalink: /projects/fax
author_profile: true
comments: false
---

This application is a wrapper around a MS-DOS based fax program and provides a set of fax templates for a fixed set of recipients.

The application 'Telefax 1.0' can be seen as an extension to a MS-DOS based fax software.

Recipient addresses are stored in a plain text file from which they are loaded by the application upon start-up. Each record is assigned to a function key (F1-F10). That makes the application somewhat restrictive, but the requirement was that only a few business partners should receive faxes.

[![Main Menu]({{"/assets/images/telefaxmainsmall.png" | absolute_url}})]({{"/assets/images/telefaxmain.png" | absolute_url}})  
*Main Menu*

An empty template offers the possibility to enter address data manually in case some other recipient has to be addressed.

[![Empty Template]({{"/assets/images/telefaxadressesmall.png" | absolute_url}})]({{"/assets/images/telefaxadresse.png" | absolute_url}})  
*Empty Template*

One category out of four must be selected for a classification of the fax.

[![Text Input]({{"/assets/images/telefaxtextsmall.png" | absolute_url}})]({{"/assets/images/telefaxtext.png" | absolute_url}})  
*Text Input*

Finally the fax is sent via a command-line software, that came with the modem.

|||
| --- | --- | 
| Type | Freelancer Project | 
| Environment | MS-DOS, Turbo Pascal 6.0 | 
| Link | [Telefax.tar.bz2]({{"/assets/files/telefax.tar.bz2" | absolute_url}}) (23kB) | 