---
title:  "Energy Cost Calculation"
layout: single
permalink: /projects/eccalc
author_profile: true
comments: false
---

An application performing a cost comparison between a traditional power supply and a power generator.

The application 'Energy Cost Calculation' was used by the sales department of a company selling electrical generators and stand-by-sets. The usual task of a sales representative was to make a comparison on the effectivity and the savings of an electrical generator, compared to standard electrical power supply. This application automates most of the tasks that had to be done manually before.

[![Main Screen 1/4]({{"/assets/images/wirtberemain1small.png" | absolute_url}})]({{"/assets/images/wirtberemain1.png" | absolute_url}})  
*Main Screen 1/4*

The first screen is asking for the customer's address and the electrical power consumption over a given period of time.

[![Main Screen 2/4]({{"/assets/images/wirtberemain2small.png" | absolute_url}})]({{"/assets/images/wirtberemain2.png" | absolute_url}})  
*Main Screen 2/4*

In the second screen the monthly consumption values of the customer have to be entered. Additionally any extra power bought in the specified period has to be entered.

[![Main Screen 3/4]({{"/assets/images/wirtberemain3small.png" | absolute_url}})]({{"/assets/images/wirtberemain3.png" | absolute_url}})  
*Main Screen 3/4*

On the third screen the customer's thermal data, like oil or gas consumption, is taken into account. Because the generator produces heat while running and this heat is reused for heating purposes, these values must be included in the calculation as well.

[![Main Screen 4/4]({{"/assets/images/wirtberemain4small.png" | absolute_url}})]({{"/assets/images/wirtberemain4.png" | absolute_url}})  
*Main Screen 4/4*

On the fourth screen the data of the electrical generator has to be chosen in order to proceed to the final calculation.

The application is able to load, store, and print the customer data together with the calculations.

[![Overview]({{"/assets/images/wirtberekalk1small.png" | absolute_url}})]({{"/assets/images/wirtberekalk1.png" | absolute_url}})  
*Overview*

[![Balance]({{"/assets/images/wirtberekalk2small.png" | absolute_url}})]({{"/assets/images/wirtberekalk2.png" | absolute_url}})  
*Balance*

The preferences screen gives access to configuration values like the data path and other parameters.

[![Preferences]({{"/assets/images/wirtberefestwertesmall.png" | absolute_url}})]({{"/assets/images/wirtberefestwerte.png" | absolute_url}})  
*Preferences*

|||
| --- | --- | 
| Type | Freelancer Project | 
| Environment | MS-DOS, Turbo Pascal 6.0 | 
| Link | [wkbekb.tar.bz2]({{"/assets/files/wkbekb.tar.bz2" | absolute_url}}) (130kB) | 