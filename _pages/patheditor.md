---
title:  "Path Editor"
layout: single
permalink: /projects/patheditor
author_profile: true
comments: false
---

A graphical user-interface to define pathways for a vehicle with real-time control features.

This application was developed during a practical course on the topic of real-time systems at Saarland University. The assignment of the course was to develop a vehicle with a real-time controller system. My part was the GUI editor to create paths the vehicle should follow. Nikolaus Gerteis was responsible for the hardware and Roland Schlosser for the micro-controller application.

The micro-controller 80C166 with its various IO channels is used to implement the fundamental real-time principle of 'Input, Processing, and Output'.

Input:  
The controller is attached to a photo-sensor that is measuring the distance for each of the two tires. This is done the same way as for a computer mouse, a wheel with wholes which trigger the photo-sensor regularly, is used.

Processing:  
The application on the controller uses the input signals to calculate a distance which is then compared to the track information, loaded to the vehicle at startup.

Output:  
If a gap is detected, the controller issues commands to the two output channels, each one connected to one of the vehicle engines in order to correct the detected gap.

The application PathEditor is used for drawing the tracks that can be loaded to the vehicle via a serial connection. Once the data is transmitted, the vehicle can be detached from the cable and the execution of the program starts.

[![Front View]({{"/assets/images/fahrzeug1small.png" | absolute_url}})]({{"/assets/images/fahrzeug1.png" | absolute_url}})  
*Front View*

[![Top View]({{"/assets/images/fahrzeug2small.png" | absolute_url}})]({{"/assets/images/fahrzeug2.png" | absolute_url}})  
*Top View*

[![Side View]({{"/assets/images/fahrzeug3small.png" | absolute_url}})]({{"/assets/images/fahrzeug3.png" | absolute_url}})  
*Side View*

[![Path Editor]({{"/assets/images/streckeneditorsmall.png" | absolute_url}})]({{"/assets/images/streckeneditor.png" | absolute_url}})  
*Path Editor*

By pressing the left mouse button, track elements can be defined. After a line element a curve element is automatically selected next. The right mouse button has an 'undo' functionality assigned, where the definition of the last track element can be reverted. The application has additional features for loading and storing tracks.

In order to transfer the data to the vehicle it must be attached to a computer via a serial line cable. The track data is generated into a C source file from which it is cross-compiled, using the GCC for the Siemens 80C166 micro-controller. After successful compilation the GDB remote debugger is started with which the application is loaded via the serial line to the micro-controller on the vehicle.

To start the vehicle following the uploaded track it must be detached from the computer and an impulse on the wheels' photo sensor must be given to start the actual execution of the track.

|||
| --- | --- | 
| Type | University Project | 
| Environment | Solaris, XWindows, LEDA, vi, GCC (C++, C-Cross-Compiler for 80C166), Make | 
| Link | [fopra.tar.bz2]({{"/assets/files/fopra.tar.bz2" | absolute_url}}) (1692kB) | 