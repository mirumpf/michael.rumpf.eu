---
title:  "Allowance"
layout: single
permalink: /projects/allowance
author_profile: true
comments: false
---

This application simplifies allowance calculation on construction sites by replacing manual calculations on paper forms with computer based calculations.

At program startup the user has to select a construction site in order to proceed to the main menu.

[![Main Menu]({{"/assets/images/aufmassmainsmall.png" | absolute_url}})]({{"/assets/images/aufmassmain.png" | absolute_url}})
*Main Menu*

The first ten menu items provide functions for the collection of allowances. Each mask is preceded by a blueprint number and the resulting item number for the allowance.

[![Information]({{"/assets/images/aufmassinfosmall.png" | absolute_url}})]({{"/assets/images/aufmassinfo.png" | absolute_url}})
*Information*

[![Input mask for length, width, and height]({{"/assets/images/aufmasslbhsmall.png" | absolute_url}})]({{"/assets/images/aufmasslbh.png" | absolute_url}})
*Input mask for length, width, and height*

The "daily reports" provide information about the human resource usage and the weather conditions for the specified day.

[![Daily report mask 1]({{"/assets/images/aufmasstagber1small.png" | absolute_url}})]({{"/assets/images/aufmasstagber1.png" | absolute_url}})
*Daily report mask 1*

[![Daily report mask 2]({{"/assets/images/aufmasstagber2small.png" | absolute_url}})]({{"/assets/images/aufmasstagber2.png" | absolute_url}})
*Daily report mask 2*

The administration menu provides printing, faxing, copying, or removing of allowance reports.

[![File administration]({{"/assets/images/aufmassverwaltsmall.png" | absolute_url}})]({{"/assets/images/aufmassverwalt.png" | absolute_url}})
*File administration*

The difference between the versions from 1.0 to 5.2 is the number of supported allowance types, the daily report functionality, and the file administration feature.

|  | | 
| --- | --- | 
| Role | Architect, Developer | 
| Environment | MS-DOS, Turbo Pascal 3.0 - 6.0 | 
| Link | [aufmass.tar.bz2]({{"/assets/files/aufmass.tar.bz2" | absolute_url}}) (110kB) |