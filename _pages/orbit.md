---
title:  "Orbit Resource"
layout: single
permalink: /projects/orbit
author_profile: true
comments: false
---

A link collection which was created while doing research about the ORBit CORBA ORB.

The CORBA ORB 'ORBit' has a large number of spin-off projects, with information spread over many different web-sites. That was the reason to create an [ORBit resource page](https://web.archive.org/web/20170815195940/http://orbit-resource.sourceforge.net/) on SourceForge that serves as a link collection to all those projects.

[![ORBit Resource Collection]({{"/assets/images/orbit-resource_small.png" | absolute_url}})]({{"/assets/images/orbit-resource.png" | absolute_url}})  
*ORBit Resource Collection*

Because of the new ORBit2 project the interest in the old ORBit version is vanishing, thus the page is not maintained anymore.

|||
| --- | --- | 
| Type | Personal Project | 
| Environment | Bluefish, Apache, HTML | 
| Link | [ORBit Resource](https://web.archive.org/web/20170815195940/http://orbit-resource.sourceforge.net/) | 